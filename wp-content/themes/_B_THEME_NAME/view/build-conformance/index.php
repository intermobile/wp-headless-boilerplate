<?php

use Theme\Helpers\ThemeUtils;

$deploy_webhook_url = get_option( 'url_webhook' );
$website_link_url   = get_option( 'link_website' );
$average_build_time = get_option( 'average_build_time' );
$faqs               = array(
	array(
		'title'   => 'How long until the website to update?',
		'content' => "<p>The build usually takes up to <b>{$average_build_time} minutes</b> to complete. After this time, check the website to see if your changes are live.</p>
			<p>Please wait for at least <b>10 minutes</b> to try requesting another build. Whenever you request a build, it is added to a queue, and it only start once the last build is completed.</p>",
	),
	array(
		'title'   => "What if my changes don't appear on the site?",
		'content' => "<p>If after 2 build attempts your changes still don't appear on the site, it's likely that the build is not completing due to an unexpected issue. The conformance checking tries to forecast the most known issues that might break the build process, but there can always be some unexpected ones (from editorial or code). In this case, try reaching the dev team so that they can investigate the issue.</p>
			<p>Please do not request more builds until the problem is fixed.</p>",
	),
	array(
		'title'   => 'What is the build process?',
		'content' => '<p>In a nutshell, the build process is a moment where the server reads all the CMS content and the back-end code to build each needed page of the site, and, if no issue is found, it deploys the new pages to the public server.</p>
			<p>Building all the pages into static files and storing them into a CDN helps to reduce the time to load these pages on the browser as the server doesn\'t need to process everything again on every visit.</p>
			<p>This method is more common for headless websites, which use modern frameworks and platforms to handle this process.</p>',
	),
	array(
		'title'   => 'Can I schedule a build?',
		'content' => '<p>The only way to schedule a build at the moment is by <b>scheduling a post</b>. Once the shceduled post gets published, a build will automatically be requested.</p>
			<p>However, if there are any issues with the new posts, the build will fail. To prevent this, include scheduled posts on the checking and make sure these posts pass all mandatory tests.</p>
			<p>If for some reason this behavior is not working as expected, please contact the dev team with any useful info.</p>',
	),
	array(
		'title'   => 'Can more tests be added to the checking?',
		'content' => '<p>Yes, anyone can request a new test to be added.</p>
			<p>The conformance check has 2 main goals: to forecast issues that could cause an error to the build process, and to guarantee some best practices are being followed in the content. Any rule that is important for the editorial team to follow and can be tested automatically, can be added to the checking.</p>
			<p>If you want to suggest a new test to the added, get in contact with the dev team to evaluate its implementation.</p>',
	),
)

?>
<div id="build-conformance-page" class="wrap" :class="{ loaded: status }"
	data-endpoint="<?php echo get_rest_url() . 'admin/v1/build-conformance?token=' . wp_get_session_token(); ?>">

	<!-- HEADING -->
	<h1 class="wp-heading-inline">Build Conformance</h1>
	<p class="info-message" :class="[`info-message--${status}`]">
		<span class="spinner is-active" v-if="isStatus(['checking','deploying'])"></span>
		<span v-html="infoMessage"></span>
	</p>

	<!-- ERROR MESSAGE -->
	<div v-if="isStatus(['check_failed','deploy_failed'])">
		<div class="card-error">
			<p>
				An error has occurred with the request. Try again later, or contact the development team.
			</p>
			<p class="error-response">
				<strong>Error message:</strong> <span v-html="errorMessage"></span>
			</p>
		</div>
	</div>

	<!-- REPROVED RESULTS -->
	<section v-if="reprovedTests.length > 0">
		<h2>Reproved:</h2>
		<?php
		ThemeUtils::get_template(
            'view/build-conformance/components/accordions',
            array(
				'accordion_type' => 'error',
				'list_var_name'  => 'reprovedTests',
				'icon_class'     => 'dashicons-dismiss',
            )
		);
		?>
	</section>

	<!-- WARNING RESULTS -->
	<section v-if="warningTests.length > 0">
		<h2>Needs improvement:</h2>
		<?php
		ThemeUtils::get_template(
            'view/build-conformance/components/accordions',
            array(
				'accordion_type' => 'warning',
				'list_var_name'  => 'warningTests',
				'icon_class'     => 'dashicons-warning',
            )
		);
		?>
	</section>

	<!-- BUTTONS -->
	<div class="action-buttons" v-if="!isStatus('deploy_completed')">
		<button class="button button-primary" v-on:click="runTests()" :disabled="!isCheckEnabled">
			{{ hasCheckedOnce ? 'Check again' : 'Run build checking' }}
		</button>

		<?php if ( $deploy_webhook_url ) : ?>
		<button class="button button-primary button-black" id="button-build" v-on:click="triggerDeploy()"
			:disabled="!isDeployEnabled">
			Build & Deploy
		</button>
		<?php endif; ?>
		<?php if ( $deploy_webhook_url ) : ?>
		<p v-if="isDeployEnabled">
			⚠ Use the <strong>Build & Deploy</strong> button with moderation! And read the <b>FAQs</b> after the build
			for more info.
		</p>
		<?php endif; ?>
		<p v-if="isCheckEnabled" class="post-status-checkboxes">
			<span>Test posts with status:</span>
			<label>
				<input type="checkbox" disabled checked>
				<span>Published</span>
			</label><br>
			<label>
				<input type="checkbox" v-model="includeScheduledPosts">
				<span>Scheduled</span>
			</label><br>
			<label>
				<input type="checkbox" v-model="includePendingPosts">
				<span>Pending Review</span>
			</label>
		</p>
	</div>

	<!-- APPROVED RESULTS -->
	<section v-if="approvedTests.length > 0" style="margin-top: 4em">
		<h2>Passed tests:</h2>
		<?php
		ThemeUtils::get_template(
            'view/build-conformance/components/accordions',
            array(
				'accordion_type' => 'success',
				'list_var_name'  => 'approvedTests',
				'icon_class'     => 'dashicons-yes-alt',
            )
		);
		?>
	</section>

	<!-- DEPLOY MESSAGE & FAQs -->
	<div v-if="status == 'deploy_completed'">
		<div class="build-requested-successfully">
			<?php foreach ( $faqs as $index => $faq ) : ?>
			<div class="accordion accordion--faq"
				:class="{ expanded: expandedAccordionId == 'faq-<?php echo $index; ?>' }">

				<!-- ACCORTION TOGGLE -->
				<button class="accordion-toggle" v-on:click="toggleAccordion('faq-<?php echo $index; ?>')">

					<i class="dashicons dashicons-editor-help"></i>
					<span class="accordion-toggle-label"><?php echo $faq['title']; ?></span>
					<span class="accordion-toggle-action expandable"></span>
				</button>

				<!-- ACCORDION CONTENT -->
				<div class="accordion-content">
					<?php echo $faq['content']; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>

		<p class="action-buttons">
			<button class="button button-primary button-with-icon" v-on:click="returnToBeginning()">
				<span>
					<span class="dashicons dashicons-arrow-left-alt"></span>
					Return
				</span>
			</button>
			<?php if ( $website_link_url ) : ?>
			<a href="<?php echo $website_link_url; ?>" target="_blank" title="Open website in a new tab"
				class="button button-secondary button-with-icon">
				<span>
					Visit website
					<span class="dashicons dashicons-external"></span>
				</span>
			</a>
			<?php endif; ?>
		</p>
	</div>

	<!-- SETTINGS -->
	<input type="hidden" id="deploy-webhook-url" value="<?php echo $deploy_webhook_url; ?>">

	<!-- Make sure the GIF will be already loaded when required -->
	<img src="<?php echo get_admin_url(); ?>/images/spinner.gif" width="1" height="1" style="opacity:0;" />
</div>