<div id="build-settings" class="wrap">
	<h1 class="wp-heading-inline">Build Conformance Settings</h1>
	<form method="post" action="options.php">
		<?php wp_nonce_field( 'update-options' ); ?>
		<p>
			<strong>Deploy Webhook URL:</strong><br />
			<input type="text" name="url_webhook" size="100" value="<?php echo get_option( 'url_webhook' ); ?>" />
		</p>
		<p>
			<strong>Website Link URL:</strong><br />
			<input type="text" name="link_website" size="100" value="<?php echo get_option( 'link_website' ); ?>" />
		</p>
		<p>
			<strong>Average Build Time:</strong><br />
			<input type="number" name="average_build_time" size="5"
				value="<?php echo get_option( 'average_build_time' ); ?>" />
		</p>
		<p>
			<input type="submit" name="Submit" value="Update" class="button button-primary" />
		</p>

		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="url_webhook,link_website,average_build_time" />
	</form>
</div>