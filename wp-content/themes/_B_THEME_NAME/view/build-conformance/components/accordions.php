<div class="accordion accordion--<?php echo $accordion_type; ?>" v-for="(test,index) in <?php echo $list_var_name; ?>"
	:class="{ expanded: expandedAccordionId == test.id, faded: (status == 'checking' || status == 'deploying') }">

	<!-- ACCORTION TOGGLE -->
	<button class="accordion-toggle" v-on:click="toggleAccordion(test.id)"
		<?php echo 'success' === $accordion_type ? 'disabled' : ''; ?>>

		<i class="dashicons <?php echo $icon_class; ?>"></i>
		<span class="accordion-toggle-label" v-html="test.description"></span>
		<span class="accordion-toggle-count" v-if="test.results.length">{{ test.results.length }}</span>

		<?php if ( 'success' !== $accordion_type ) : ?>
		<span class="accordion-toggle-action expandable">See details</span>
		<?php else : ?>
		<span class="accordion-toggle-action">Passed</span>
		<?php endif; ?>
	</button>

	<!-- ACCORDION CONTENT -->
	<?php if ( 'success' !== $accordion_type ) : ?>
	<div class="accordion-content">
		<p class="accordion-content-message" v-if="test.message" v-html="test.message"></p>

		<h3>Reproved items:</h3>
		<ul>
			<li v-for="item in test.results">
				<span v-html="item.title"></span>
				<a :href="item.edit_link" target="_blank" :tabindex="expandedAccordionId === test.id ? 0 : -1">
					Edit<i class="dashicons dashicons-external"></i>
				</a>
			</li>
		</ul>
	</div>
	<?php endif; ?>
</div>