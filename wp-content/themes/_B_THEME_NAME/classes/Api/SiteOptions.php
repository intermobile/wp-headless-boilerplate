<?php

namespace Theme\Api;

use Solidify\Core\Hook;
use Theme\Helpers\AcfUtils;

/**
 * Register a custom endpoint for returning the site configurations
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
 */
class SiteOptions extends Hook {
	public function __construct() { // phpcs:ignore
		$this->add_action( 'rest_api_init', 'register_options_endpoint' );
	}

	/**
	 * Add a field returning a list of related posts
	 */
	public function register_options_endpoint() {
		register_rest_route(
            'site/v1',
            '/options',
            array(
				'methods'             => 'GET',
				'callback'            => function() {
					return array(
						'options' => AcfUtils::get_formatted_fields( 'options' ),
					);
				},
				'permission_callback' => '__return_true',
            )
        );
	}
}