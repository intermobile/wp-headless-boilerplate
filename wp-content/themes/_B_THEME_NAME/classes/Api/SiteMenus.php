<?php

namespace Theme\Api;

use Solidify\Core\Hook;
use Theme\Helpers\ThemeUtils;

/**
 * Register a custom endpoint for returning the menu items
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
 */
class SiteMenus extends Hook {
	public function __construct() { // phpcs:ignore
		$this->add_action( 'rest_api_init', 'register_menus_endpoint' );
	}

	/**
	 * Add a field returning an array with all registered menus associated with a menu location
	 */
	public function register_menus_endpoint() {
		register_rest_route(
            'site/v1',
            '/menus',
            array(
				'methods'             => 'GET',
				'callback'            => function() {
					return $this->get_nav_menus();
				},
				'permission_callback' => '__return_true',
            )
        );
	}

	/**
	 * Returns data from all registered menus locations
	 *
	 * @return array
	 */
	private function get_nav_menus() {
		$registered_locations = get_registered_nav_menus();
		$menu_locations       = get_nav_menu_locations();
		$menus                = array();
		foreach ( $registered_locations as $key => $name ) {
			if ( isset( $menu_locations[ $key ] ) ) {
				$menu_id     = $menu_locations[ $key ];
				$menu_object = wp_get_nav_menu_object( $menu_id );
				$menu_items  = wp_get_nav_menu_items( $menu_id );
				if ( $menu_object && $menu_items ) {
					$menu_data = array(
						'id'       => $menu_object->term_id,
						'name'     => $menu_object->name,
						'slug'     => $menu_object->slug,
						'location' => $key,
						'count'    => $menu_object->count,
						'items'    => $this->get_menu_items_hierarchically( $menu_items ),
					);
					$menus[]   = $menu_data;
				}
			}
		}
		return $menus;
	}

	/**
	 * Returns menu items stacked hierarchically
	 *
	 * @param array      $menu_items All menu items from wp_get_nav_menu_items().
	 * @param string|int $parent_item_id ID of the parent menu item to get items from, default is 0 (no parent).
	 *
	 * @return array
	 */
	private function get_menu_items_hierarchically( $menu_items, $parent_item_id = 0 ) {
		$items = array();
		foreach ( $menu_items as $menu_item ) {
			if ( intval( $parent_item_id ) === intval( $menu_item->menu_item_parent ) ) {
				$items[] = $this->get_menu_item( $menu_item, $menu_items );
			}
		}
		return $items;
	}

	/**
	 * Returns formatted data from a menu item
	 *
	 * @param WP_Post $menu_item Raw data from a menu item.
	 * @param array   $menu_items All menu items from wp_get_nav_menu_items().
	 *
	 * @return array
	 */
	private function get_menu_item( $menu_item, $menu_items ) {
		$item = array(
			'id'          => $menu_item->ID,
			'url'         => trim( ThemeUtils::get_relative_path( $menu_item->url ) ),
			'title'       => $menu_item->title,
			'attr_title'  => $menu_item->attr_title,
			'class'       => join( ' ', $menu_item->classes ),
			'target'      => $menu_item->target,
			'rel'         => $menu_item->xfn,
			'child_items' => $this->get_menu_items_hierarchically( $menu_items, $menu_item->ID ),
			'object_id'   => $menu_item->object_id,
			'object_type' => $menu_item->object,
		);
		return $item;
	}
}