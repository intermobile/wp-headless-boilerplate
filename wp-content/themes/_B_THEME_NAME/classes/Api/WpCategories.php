<?php

namespace Theme\Api;

use Solidify\Core\Hook;
use Theme\Helpers\ThemeUtils;
use Theme\Pages\CategoryPageData;
use Theme\Pages\SubcategoryPageData;

/**
 * Extends the native /wp-json/wp/v2/categories/ endpoint with more data
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/
 */
class WpCategories extends Hook {
	private $object_type = 'category';

	public function __construct() { // phpcs:ignore
		$this->add_action( 'rest_api_init', 'add_layout_field' );
		$this->add_action( 'rest_api_init', 'add_path_field' );
		$this->add_action( 'rest_api_init', 'add_page_data_field' );
	}

	/**
	 * Returns a fixed layout value for the category
	 */
	public function add_layout_field() {
		register_rest_field(
            $this->object_type,
            'layout',
            array(
				'get_callback' => function( $categ ) {
					$category = get_category( $categ['id'] );
					return $category->parent ? 'subcategory' : 'category';
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the category relative URL path
	 */
	public function add_path_field() {
		register_rest_field(
            $this->object_type,
            'path',
            array(
				'get_callback' => function( $category ) {
					return ThemeUtils::get_relative_path( get_category_link( $category['id'] ) );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the page ACF fields data
	 */
	public function add_page_data_field() {
		register_rest_field(
            $this->object_type,
            'page_data',
            array(
				'get_callback' => function( $category ) {
					$category_data = get_category( $category['id'] );
					if ( 0 === $category_data->parent ) {
						return new CategoryPageData( $category['id'] );
					} else {
						return new SubcategoryPageData( $category['id'] );
					}
				},
				'schema'       => null,
            )
	 	);
	}
}
