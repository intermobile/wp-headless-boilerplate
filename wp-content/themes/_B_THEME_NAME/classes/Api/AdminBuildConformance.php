<?php

namespace Theme\Api;

use Solidify\Core\Hook;
use Theme\Helpers\TestObject;

/**
 * Register a custom endpoint to return the conformance test to enable the Build function with more probability of success.
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
 */
class AdminBuildConformance extends Hook {

	/**
	 * Array of test responses.
	 *
	 * @var array
	 */
    private $responses = array();

	/**
	 * AdminBuildConformance constructor.
	 */
	public function __construct() {
		$this->add_action( 'rest_api_init', 'register_build_conformance_endpoint' );
	}

	/**
	 * Register the endpoint for returning array of responses from each test
	 */
	public function register_build_conformance_endpoint() {
		register_rest_route(
            'admin/v1',
            '/build-conformance',
            array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => function( \WP_REST_Request $data ) {
					if ( wp_get_session_token() === $data->get_param( 'token' ) ) {
						// Get additional args for the tests
						$args = array();
						if ( null !== $data->get_param( 'post_status' ) ) {
							$args['post_status'] = explode( ',', $data->get_param( 'post_status' ) );
						}

						// Adding each test
						$this->add_test( 'check_correct_categorization', 'post', $args );
						$this->add_test( 'check_primary_category', 'post', $args );
						$this->add_test( 'check_featured_image', 'post', $args );
						$this->add_test( 'check_uncategorized_articles', 'post', $args );
						$this->add_test( 'check_content_scripts', 'post', $args );
						foreach ( array( 'post', 'category', 'page' ) as $context ) {
							$this->add_test( 'check_seo_meta_description', $context, $args );
						}
					}
					// Return all tests
					return $this->responses;
				},
				'permission_callback' => '__return_true',
            )
        );
	}

	/**
	 * Add tests dynamically using a context and a function call
	 *
	 * @param string $function Function of test.
	 * @param string $context Context/class representing the test.
	 * @param array  $args Additional arguments to pass to the test functions.
	 */
    private function add_test( string $function, string $context, array $args = array() ) {
		try {
			$class    = 'Theme\\Tests\\' . str_replace( '-', '', ucwords( $context, '-' ) ) . 'Tests';
			$instance = new $class();
			$response = $instance->$function( $args );
			if ( $response instanceof TestObject ) {
				$this->responses[] = $response;
			}
		} catch ( \Exception $error ) {
			echo $error;
			exit;
		}
    }
}