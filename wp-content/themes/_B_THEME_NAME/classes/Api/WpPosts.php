<?php

namespace Theme\Api;

use Solidify\Core\Hook;
use Theme\Helpers\ArticleQuery;
use Theme\Helpers\ArticleUtils;
use Theme\Helpers\TaxonomyObject;
use Theme\Helpers\ThemeUtils;

/**
 * Extends the native /wp-json/wp/v2/posts/ endpoint with more data
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/
 */
class WpPosts extends Hook {
	private $object_type = 'post';

	public function __construct() {
		$this->add_action( 'rest_api_init', 'add_extract_field' );
		$this->add_action( 'rest_api_init', 'add_image_field' );
		$this->add_action( 'rest_api_init', 'add_primary_category_field' );
		$this->add_action( 'rest_api_init', 'add_layout_field' );
		$this->add_action( 'rest_api_init', 'add_author_field' );
		$this->add_action( 'rest_api_init', 'add_path_field' );
		$this->add_action( 'rest_api_init', 'add_date_formatted_field' );
		$this->add_action( 'rest_api_init', 'add_page_data_field' );
	}

	/**
	 * Returns the excerpt real value, as empty if not defined
	 */
	public function add_extract_field() {
		register_rest_field(
            $this->object_type,
            'extract',
            array(
				'get_callback' => function( $post ) {
					return has_excerpt( $post['id'] ) ? get_the_excerpt( $post['id'] ) : '';
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the featured image details
	 */
	public function add_image_field() {
		register_rest_field(
            $this->object_type,
            'image',
            array(
				'get_callback' => function( $post ) {
					return ArticleUtils::get_post_thumbnail_data( $post['id'] );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Adds a field with the post category data
	 */
	public function add_primary_category_field() {
		register_rest_field(
            $this->object_type,
            'primary_category',
            array(
				'get_callback' => function( $post ) {
					$category = ArticleUtils::get_post_primary_category( $post['id'] );
					return $category->term_id; // new TaxonomyObject( $category );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns a fixed layout value for the post
	 */
	public function add_layout_field() {
		register_rest_field(
            $this->object_type,
            'layout',
            array(
				'get_callback' => function( $post ) {
					return 'post';
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns author data for the post
	 */
	public function add_author_field() {
		register_rest_field(
            $this->object_type,
            'author_data',
            array(
				'get_callback' => function( $post ) {
					return ArticleUtils::get_post_author_data( $post['id'] );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the post relative URL path
	 */
	public function add_path_field() {
		register_rest_field(
            $this->object_type,
            'path',
            array(
				'get_callback' => function( $post ) {
					return ThemeUtils::get_relative_path( get_the_permalink( $post['id'] ) );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the date post formatted
	 */
	public function add_date_formatted_field() {
		register_rest_field(
            $this->object_type,
            'date_formatted',
            array(
				'get_callback' => function( $post ) {
					return get_the_date( 'F j, Y', $post['id'] );
				},
				'schema'       => null,
            )
	 	);
	}
	/**
	 * Returns author data and AFC's for the post
	 */
	public function add_page_data_field() {
		register_rest_field(
            $this->object_type,
            'page_data',
            array(
				'get_callback' => function( $post ) {
					$primary_category = ArticleUtils::get_post_primary_category( $post['id'] );
					return array(
						'breadcrumbs'            => ArticleUtils::get_post_breadcrumbs( $post['id'] ),
						'modified_formatted'     => get_the_modified_date( '', $post['id'] ),
						'estimated_reading_time' => ArticleUtils::get_post_reading_time( trim( get_the_content( null, false, $post['id'] ) ) ),
						'related_articles'       => ArticleQuery::get_related_articles( $primary_category->term_id, 6, array( $post['id'] ) ),
					);
				},
				'schema'       => null,
            )
		);
	}
}
