<?php

namespace Theme\Api;

use Solidify\Core\Hook;
use Theme\Helpers\PageUtils;
use Theme\Helpers\ThemeUtils;
use Theme\Pages;

/**
 * Extends the native /wp-json/wp/v2/pages/ endpoint with more data
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/
 */
class WpPages extends Hook {
	private $object_type = 'page';

	public function __construct() { // phpcs:ignore
		$this->add_action( 'rest_api_init', 'add_front_page_field' );
		$this->add_action( 'rest_api_init', 'add_layout_field' );
		$this->add_action( 'rest_api_init', 'add_path_field' );
		$this->add_action( 'rest_api_init', 'add_modified_formatted_field' );
		$this->add_action( 'rest_api_init', 'add_page_data_field' );
	}

	/**
	 * Returns whether the page is the front page or not
	 */
	public function add_front_page_field() {
		register_rest_field(
            $this->object_type,
            'is_front_page',
            array(
				'get_callback' => function( $page ) {
					return intval( get_option( 'page_on_front' ) ) === $page['id'];
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the simplified template name
	 */
	public function add_layout_field() {
		register_rest_field(
            $this->object_type,
            'layout',
            array(
				'get_callback' => function( $page ) {
					return PageUtils::get_page_layout( $page['id'] );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the page relative URL path
	 */
	public function add_path_field() {
		register_rest_field(
            $this->object_type,
            'path',
            array(
				'get_callback' => function( $page ) {
					return ThemeUtils::get_relative_path( get_the_permalink( $page['id'] ) );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the page's modified date, formatted accordingly to the date_format options
	 */
	public function add_modified_formatted_field() {
		register_rest_field(
            $this->object_type,
            'modified_formatted',
            array(
				'get_callback' => function( $page ) {
					return get_the_modified_date( get_option( 'date_format' ), $page['id'] );
				},
				'schema'       => null,
            )
	 	);
	}

	/**
	 * Returns the page ACF fields data
	 */
	public function add_page_data_field() {
		register_rest_field(
            $this->object_type,
            'page_data',
            array(
				'get_callback' => function( $page ) {
					$layout = PageUtils::get_page_layout( $page['id'] );
					switch ( $layout ) {
						case 'home':
							return new Pages\HomePageData( $page['id'] );
						default:
							return new Pages\DefaultPageData( $page['id'] );
					}
				},
				'schema'       => null,
            )
	 	);
	}
}
