<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Hooks for core theme definitions
 */
class Theme extends Hook {
	public function __construct() {
        $this->add_action( 'after_setup_theme', 'setup_theme' );
		$this->add_action( 'init', 'exclude_pages_from_search', 99 );
	}

	/**
	 * Set up some basic definitions of the theme
	 */
	public function setup_theme(): void {
		// Make theme available for translation.
		// Translations can be filed in the /languages/ directory.
		load_theme_textdomain( '_B_THEME_NAME', get_template_directory() . '/languages' );

		// Register menu locations.
		register_nav_menus(
            array(
				'header' => __( 'Site Navigation', '_B_THEME_NAME' ),
				'social' => __( 'Social Media', '_B_THEME_NAME' ),
				'footer' => __( 'Footer Links', '_B_THEME_NAME' ),
            )
        );

		// Switch default core markup for search form, gallery and image captions to output valid HTML5.
		add_theme_support(
            'html5',
            array( 'caption' )
        );

		// Add featured image support for posts
		add_theme_support( 'post-thumbnails' );
	}

	/**
	 * Remove pages from search functionality
	 */
	public function exclude_pages_from_search() {
		global $wp_post_types;
		$wp_post_types['page']->exclude_from_search = true;
	}
}
