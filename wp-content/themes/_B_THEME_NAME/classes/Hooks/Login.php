<?php

namespace Theme\Hooks;

use Theme\Helpers\ImageObject;
use Solidify\Core\Hook;
use Theme\Helpers\Enqueues;

/**
 * Hooks for customizations to the CMS login page
 */
class Login extends Hook {
	public function __construct() {
		$this->add_action( 'login_head', 'custom_login_favicon' );
		$this->add_action( 'login_enqueue_scripts', 'enqueue_login_styles' );
		$this->add_action( 'login_enqueue_scripts', 'login_logo_image' );
		$this->add_filter( 'login_headerurl', 'login_logo_url' );
		$this->add_filter( 'login_headertext', 'login_logo_title' );
		$this->add_action( 'login_form', 'disable_login_autocomplete' );
	}

	/**
	 * Define custom favicon for the login page
     */
	public static function custom_login_favicon() {
		AdminCustomization::custom_admin_favicon();
	}

	/**
	 * Enqueue styles to the login page
	 */
	public function enqueue_login_styles() {
		wp_enqueue_style(
			'admin-login-styles',
            Enqueues::get_css_uri( 'login' ),
			array(),
			filemtime( Enqueues::get_css_path( 'login' ) )
		);
	}

	/**
	 * Define the URL of the image to show in the login page
	 */
	public function login_logo_image(): void {
		$logo_path = ImageObject::get_image_uri_from_assets( '_B_LOGO_NAME.svg' );
		echo "<style>#login h1 a,.login h1 a{background-image: url({$logo_path})}</style>\n";
	}

	/**
	 * Change the Login Logo URL
	 */
	public function login_logo_url(): string {
		return home_url();
	}

	/**
	 * Change the Login Logo Title
	 */
	public function login_logo_title(): string {
		return get_bloginfo( 'name' );
	}

	/**
	 * Disable input autocomplete on login form for security purposes
	 */
	public function disable_login_autocomplete() {
		$content = ob_get_contents();
		if ( ob_get_length() > 0 ) {
			ob_end_clean();
		}
		$content = str_replace( 'id="user_login"', 'id="user_login" autocomplete="off"', $content );
		echo $content;
	}
}