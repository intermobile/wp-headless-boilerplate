<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Customizations for headless preview
 */
class Preview extends Hook {
	/**
	 * Post types for which the preview is enabled
	 *
	 * @var string[]
     */
	public const POST_TYPES_WITH_PREVIEW_ENABLED = array( 'post' );

	/**
	 * Expiration time for the preview token validation (in seconds)
	 *
	 * @var string
     */
	public const PREVIEW_TOKEN_EXPIRATION = 60 * 60 * 8; // 8 hours

	/**
	 * Route for the preview handler URL
	 *
	 * @var string
     */
	public const PREVIEW_ROUTE = 'preview';

	public function __construct() { // phpcs:ignore
		$this->add_action( 'parse_request', 'parse_preview_page_request' );
		$this->add_action( 'admin_footer', 'print_preview_base_url' );
		$this->add_action( 'admin_footer', 'styles_for_enabled_preview' );
		$this->add_action( 'admin_print_footer_scripts', 'add_script_for_preview_button' );
		$this->add_filter( 'preview_post_link', 'change_post_preview_link', 10, 2 );
		$this->add_filter( 'rest_post_query', 'handle_draft_posts_return', 10, 2 );
		$this->add_filter( 'allowed_redirect_hosts', 'add_website_to_allowed_redirect_hosts', 10, 2 );

		// Customize URL for the 'View %s' links
		$this->add_filter( 'page_link', 'customize_view_link' );
		$this->add_filter( 'post_link', 'customize_view_link' );
		$this->add_filter( 'term_link', 'customize_view_link' );
		$this->add_filter( 'author_link', 'customize_view_link' );
	}

	/**
	 * Get the base url for preview page
	 */
	public static function get_preview_base_url() {
		return get_home_url( null, '/' . self::PREVIEW_ROUTE . '/' );
	}

	/**
	 * Parse the preview page request
	 *
	 * @param WP $wp WP instance.
	 */
	public function parse_preview_page_request( $wp ) {
		$pattern          = '/^\/' . preg_quote( self::PREVIEW_ROUTE, '/' ) . '\//'; // TODO: Add support to multisite
		$is_preview_route = $wp->request === self::PREVIEW_ROUTE
			|| ( isset( $_SERVER['REQUEST_URI'] ) && preg_match( $pattern, $_SERVER['REQUEST_URI'] ) );

		if ( $is_preview_route ) {
			if ( ! is_user_logged_in() && ! user_can( get_current_user_id(), 'edit_posts' ) ) {
				wp_die( __( 'Sorry, you are not allowed to access this page.', '_B_THEME_NAME' ) );
			}

			if ( ! isset( $_GET['id'] ) ) {
				wp_die( __( 'Sorry, we were unable to find the content you want to preview.<br>If you think this is an error, please contact the development team.', '_B_THEME_NAME' ) );
			}

			$post = get_post( sanitize_text_field( wp_unslash( $_GET['id'] ) ) );

			if ( ! $post ) {
				wp_die( __( 'Sorry, we were unable to find the content you want to preview.<br>If you think this is an error, please contact the development team.', '_B_THEME_NAME' ) );
			}

			// Show message if preview is not enabled for the post type
			if ( ! in_array( $post->post_type, self::POST_TYPES_WITH_PREVIEW_ENABLED, true ) ) {
				$post_type_obj = get_post_type_object( $post->post_type );
				// translators: %s: Post type name.
				wp_die( sprintf( __( 'Sorry, the <b>preview</b> functionality is not available for <b>%s</b>.', '_B_THEME_NAME' ), $post_type_obj->labels->name ) );
			}

			$site_url = get_option( 'options_website_url', '' );
			if ( ! $site_url ) {
				wp_die( __( 'Some settings are missing for the preview to work.<br>Please contact an administrator to configure the <b>Website URL</b> field.', '_B_THEME_NAME' ) );
			}

			$token            = rawurlencode( self::generate_preview_token() );
			$site_preview_url = untrailingslashit( $site_url ) . "/api/preview/?id=$post->ID&post_type=$post->post_type&token=$token";
			wp_safe_redirect( $site_preview_url );
			exit;
		}
	}

	/**
	 * Change the preview link to point to the custom headless preview page
	 *
	 * @param string $permalink The post permalink.
	 */
	public function customize_view_link( $permalink ) {
		$cms_url  = get_site_url();
		$site_url = untrailingslashit( get_option( 'options_website_url', '' ) );
		return $site_url ? str_replace( $cms_url, $site_url, $permalink ) : $permalink;
	}

	/**
	 * Change the native 'Preview' links to point to the custom headless preview page
	 *
	 * @param string  $preview_link The default preview link.
	 * @param WP_Post $post The post object.
	 */
	public function change_post_preview_link( $preview_link, $post ) {
		$preview_url = self::get_preview_base_url();
		return strpos( $preview_link, 'preview=true' ) === false ? $preview_link : "{$preview_url}?id=$post->ID";
	}

	/**
	 * Print the preview base url to be used in the frontend
	 */
	public function print_preview_base_url() {
		global $pagenow;
		if ( $pagenow === 'post-new.php' || $pagenow === 'post.php' ) {
			$preview_url = self::get_preview_base_url();
			echo "<input type='hidden' name='preview_base_url' value='$preview_url' />";
		}
	}

	/**
	 * Print styles for enabling the preview button for the post types that have it enabled
	 */
	public function styles_for_enabled_preview() {
		$classes = array();
		foreach ( self::POST_TYPES_WITH_PREVIEW_ENABLED as $post_type ) {
			$classes[] = "body.post-type-$post_type .block-editor-post-preview__button-toggle";
		}
		$selector = implode( ',', $classes );
		echo "<style>$selector{display:inline-flex;}}</style>";
	}

	/**
	 * Print script for customizing the post preview button
	 */
	public function add_script_for_preview_button() {
		global $pagenow;
		if ( $pagenow === 'post-new.php' || $pagenow === 'post.php' ) {
			?>
<script>
jQuery(document).ready(function($) {
	$(document).on('click', '.block-editor-post-preview__button-toggle', function(event) {
		event.preventDefault();
		event.stopPropagation();

		const previewUrl = $('[name="preview_base_url"]').val();
		const postId = new URLSearchParams(document.location.search).get('post');
		if (postId) {
			window.open(`${previewUrl}?id=${postId}`, '_blank');
		} else {
			alert('Please save the draft to enable the preview.');
		}
	});
});
</script>
			<?php
		}
	}

	/**
	 * Add the 'draft' post status to the query args if the preview is enabled
	 *
	 * @param array           $args Query args.
	 * @param WP_REST_Request $request Request object.
	 */
	public function handle_draft_posts_return( $args, $request ) {
		if (
			isset( $request['preview'] ) && $request['preview'] &&
			isset( $request['token'] ) && self::is_token_valid( rawurldecode( $request['token'] ) )
		) {
			global $wp_post_statuses;

			$wp_post_statuses['draft']->public = true;
			$args['post_status']               = array( 'publish', 'draft' );
		}

		return $args;
	}

	/**
	 * Add the front-end website URL to the allowed hosts to redirections
	 *
	 * @param array $hosts Allowed hosts.
	 */
	public function add_website_to_allowed_redirect_hosts( $hosts ) {
		$site_url = get_option( 'options_website_url', '' );
		if ( $site_url ) {
			$hosts[] = wp_parse_url( $site_url, PHP_URL_HOST );
		}
		return $hosts;
	}

	/**
	 * Generate a new hash to use for preview token
	 */
	public static function generate_preview_token() {
		$timestamp = time();
		return openssl_encrypt( $timestamp, 'aes-256-cbc', AUTH_KEY, 0, substr( AUTH_SALT, 0, 16 ) );
	}

	/**
	 * Validates the preview token expiration
	 *
	 * @param string $token The token to validate.
	 */
	public static function is_token_valid( $token ) {
		$timestamp  = time();
		$decrypted  = openssl_decrypt( $token, 'aes-256-cbc', AUTH_KEY, 0, substr( AUTH_SALT, 0, 16 ) );
		$expiration = $decrypted ? $decrypted + self::PREVIEW_TOKEN_EXPIRATION : 0;
		return $timestamp < $expiration;
	}
}