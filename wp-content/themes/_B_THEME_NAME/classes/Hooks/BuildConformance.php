<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Setup for a panel where a deploy can be triggered on Vercel after some content verification tests
 */
class BuildConformance extends Hook {
	public function __construct() {
		$this->add_action( 'admin_menu', 'register_admin_page' );

		if ( isset( $_GET['page'] ) && 'build-conformance' === $_GET['page'] ) {
			$this->add_action( 'admin_enqueue_scripts', 'page_enqueues' );
		}
	}

	/**
	 * Register the admin page and menu item
	 */
	public function register_admin_page() {
		if ( current_user_can( 'edit_posts' ) ) {
			add_menu_page( 'Build', 'Build', 'publish_pages', 'build-conformance', array( __CLASS__, 'page_template' ), 'dashicons-hammer', 82 );
			add_submenu_page( 'build-conformance', 'Build', 'Build', 'publish_pages', 'build-conformance', array( __CLASS__, 'page_template' ) );
		}
		if ( current_user_can( 'manage_options' ) ) {
			add_submenu_page( 'build-conformance', 'Build Settings', 'Settings', 'publish_pages', 'settings-build-conformance', array( __CLASS__, 'settings_template' ) );
		}
	}

	/**
	 * Define the content to return in the build page
	 */
	public static function page_template() {
        include_once get_template_directory() . '/view/build-conformance/index.php';
	}

	/**
	 * Define the content to return in the setting admin page
	 */
	public static function settings_template() {
        include_once get_template_directory() . '/view/build-conformance/settings.php';
	}

	/**
	 * Define the content to return to the admin page
	 */
	public static function page_enqueues() {
        wp_enqueue_script( 'axios', 'https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js', false, null, true );
		wp_enqueue_script( 'vue-js', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js', false, null, true );
		wp_enqueue_script( 'build-conformance-js', get_template_directory_uri() . '/assets/js/build-conformance.js', array( 'axios', 'vue-js' ), filemtime( get_template_directory() . '/assets/js/build-conformance.js' ), true );
		wp_enqueue_style( 'build-conformance-css', get_template_directory_uri() . '/assets/css/build-conformance.css', array(), filemtime( get_template_directory() . '/assets/css/build-conformance.css' ) );
	}
}