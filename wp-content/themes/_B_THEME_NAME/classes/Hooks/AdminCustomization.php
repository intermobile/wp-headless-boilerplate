<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;
use Theme\Api\WpPages;
use Theme\Helpers\ThemeUtils;
use Theme\Helpers\Enqueues;
use Theme\Helpers\PageUtils;

/**
 * Hooks for CMS customizations
 */
class AdminCustomization extends Hook {
	public function __construct() { // phpcs:ignore
		// Favicons
		$this->add_action( 'admin_head', 'custom_admin_favicon' );

		// Admin enqueues
		$this->add_action( 'admin_enqueue_scripts', 'enqueue_custom_admin_styles' );
		$this->add_action( 'admin_enqueue_scripts', 'enqueue_custom_admin_scripts' );

		// List columns
		$this->add_filter( 'manage_posts_columns', 'add_post_admin_custom_columns', 2 );
		$this->add_action( 'manage_posts_custom_column', 'add_post_admin_custom_column_content', 5, 2 );
		$this->add_filter( 'manage_pages_columns', 'add_page_admin_custom_columns', 2 );
		$this->add_action( 'manage_pages_custom_column', 'add_page_admin_custom_column_content', 5, 2 );

		// Menu
		$this->add_action( 'admin_menu', 'add_homepage_admin_menu_item' );

		// Color scheme
		$this->add_action( 'get_user_option_admin_color', 'override_admin_color' );
		// Remove color scheme option as it is fixed defined per environment
		remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
	}

	/**
	 * Define a custom favicon to the CMS
     */
	public static function custom_admin_favicon() {
		$favicon_url = get_stylesheet_directory_uri() . '/assets/img/_B_FAVICON.png';
		echo "<link rel=\"shortcut icon\" type=\"image/png\" href=\"{$favicon_url}\" />";
	}

	/**
	 * Enqueue styles to the admin pages
	 */
	public function enqueue_custom_admin_styles() {
		wp_enqueue_style(
			'admin-global-styles',
			Enqueues::get_css_uri( 'global' ),
			array(),
			filemtime( Enqueues::get_css_path( 'global' ) )
		);
		wp_enqueue_style(
			'admin-acf-styles',
			Enqueues::get_css_uri( 'acf' ),
			array(),
			filemtime( Enqueues::get_css_path( 'acf' ) )
		);
		wp_enqueue_style(
			'admin-info-styles',
			Enqueues::get_css_uri( 'info' ),
			array(),
			filemtime( Enqueues::get_css_path( 'info' ) )
		);
	}

	/**
     * Script to help usability of fields
     *
     * @param string $hook Name of the hooked file.
     */
    public function enqueue_custom_admin_scripts( $hook ) {
		wp_enqueue_script(
			'admin-acf-utilities',
			get_template_directory_uri() . '/assets/js/acf.js',
			array( 'jquery' ),
			filemtime( get_template_directory( '/assets/js/acf.js' ) ),
            true
        );
    }

	/**
	 * Add column for extra page data to admin listings
     *
	 * @param array $columns Listing columns.
	 * @return array
	 */
	public function add_page_admin_custom_columns( $columns ) {
		$columns['slug']   = __( 'Slug', '_B_THEME_NAME' );
		$columns['layout'] = __( 'Layout <small>(Template)</small>', '_B_THEME_NAME' );
		return $columns;
	}

	/**
	 * Add column for post thumbnail on admin listings
     *
	 * @param array $columns Listing columns.
	 * @return array
	 */
	public function add_post_admin_custom_columns( $columns ) {
		$columns['thumbnail'] = __( 'Featured Image', '_B_THEME_NAME' );
		$columns['post_id']   = __( 'ID', '_B_THEME_NAME' );
		return $columns;
	}

	/**
	 * Display extra page in the admin listing columns
     *
	 * @param array  $columns Listing columns.
	 * @param string $id Page ID.
	 */
	public function add_page_admin_custom_column_content( $columns, $id ) {
		switch ( $columns ) {
			case 'layout':
				$layout = PageUtils::get_page_layout( $id );
				echo "<code class=\"{$layout}-layout\">{$layout}</code>";
				break;
			case 'slug':
				$slug = get_post_field( 'post_name', $id );
				echo "<code>{$slug}</code>";
				break;

			default:
				break;
		}
	}

	/**
	 * Grabbing post thumbnail and displaying it in the column
     *
	 * @param array  $columns Listing columns.
	 * @param string $id Post ID.
	 */
	public function add_post_admin_custom_column_content( $columns, $id ) {
		switch ( $columns ) {
			case 'thumbnail':
				echo '<a href="' . get_post_permalink( $id ) . '" class="img-wrapper">';
				echo the_post_thumbnail( 'thumbnail' );
				echo '</a>';
				break;

			case 'post_id':
				echo "<strong class=\"post-id-column\">{$id}</strong>";
				break;

			default:
				break;
		}
	}

	/**
	 * Add item to the Home page in the main menu
	 */
	public function add_homepage_admin_menu_item() {
		foreach ( get_pages() as $key => $pages ) {
			if ( (int) get_option( 'page_on_front' ) === $pages->ID ) {
				add_menu_page(
					$pages->post_title,
					$pages->post_title,
					'edit_pages',
					'post.php?post=' . $pages->ID . '&action=edit',
					'',
					'dashicons-admin-home',
					2
				);
			}
		}
	}

	/**
	 * Override the CMS color scheme per environment
	 */
	public function override_admin_color() {
		if ( isset( $_COOKIE['wp_color_scheme'] ) ) {
			return wp_unslash( sanitize_key( $_COOKIE['wp_color_scheme'] ) );
		} elseif ( ThemeUtils::is_production() ) {
			return 'modern';
		} elseif ( ThemeUtils::is_stage() ) {
			return 'light';
		} elseif ( ThemeUtils::is_homolog() ) {
			return 'ectoplasm';
		} else {
			return 'midnight';
		}
	}
}