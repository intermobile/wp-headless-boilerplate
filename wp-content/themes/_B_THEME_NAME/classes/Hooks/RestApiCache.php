<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Hooks for configuring the WP REST Cache plugin
 */
class RestApiCache extends Hook {
	public function __construct() {
		$this->add_filter( 'wp_rest_cache/determine_object_type', 'determine_object_type', 10, 4 );
	}

	/**
	 * Informs WPRC the object type for the posts endpoint
	 *
	 * @param string $object_type Object type identified by the plugin.
	 * @param string $cache_key Key for this cache.
	 * @param string $data Data for this cache.
	 * @param string $uri Relative URI from the request.
	 */
	public function determine_object_type( $object_type, $cache_key, $data, $uri ) {
		$matches = null;
		preg_match( '/(?<=^\/wp-json)\/[\w-]+\/[\w-]+\/[\w-]+/', $uri, $matches );
		if ( 'unknown' !== $object_type || ! $matches ) {
			return $object_type;
		}

		switch ( $matches[0] ) {
			case '/wp/v2/posts':
				return 'post';

			case '/wp/v2/pages':
				return 'page';

			case '/wp/v2/categories':
				return 'category';

			case '/wp/v2/users':
				return 'user';

			case '/wp/v2/tags':
				return 'post_tag';

			case '/wp/v2/media':
				return 'attachment';

			case '/wp/v2/comments':
				return 'comment';

			default:
				return $object_type;
		}
	}
}