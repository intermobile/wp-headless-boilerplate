<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Hooks for registering additional security handlers
 */
class Security extends Hook {
	public function __construct() {
		$this->add_action( 'login_form', 'disable_login_autocomplete' );
		$this->add_action( 'after_setup_theme', 'set_csp_header' );
	}

	/**
	 * Disable autocomplete functionality in the WP login form
	 */
	public function disable_login_autocomplete() {
		$content = ob_get_contents();
		ob_end_clean();
		$content = str_replace( 'id="user_login"', 'id="user_login" autocomplete="off"', $content );
		echo $content;
	}

	/**
	 * Define the CSP settings, with a whitelist of domains allowed to execute code inside this site
	 */
	public function set_csp_header() {
		$allowed_domains = join(
            ' ',
            array(
				'https://www.youtube.com',
				'https://www.instagram.com',
				'https://www.facebook.com',
            )
        );
		header( "Content-Security-Policy: worker-src * blob:; child-src 'self' {$allowed_domains}" );
	}
}