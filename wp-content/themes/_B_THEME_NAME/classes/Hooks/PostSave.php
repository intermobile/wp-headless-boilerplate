<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;
use Theme\Helpers\ArticleUtils;

/**
 * Hooks for automating changes when saving posts
 */
class PostSave extends Hook {
	public function __construct() {
		$this->add_action( 'save_post_post', 'assign_parent_category' );
		// $this->add_action( 'save_post_post', 'assign_child_category_as_primary' );
	}

	/**
	 * Auto check the parent category if a child category is selected
     *
	 * @param string $post_id ID of the post.
	 */
	public function assign_parent_category( $post_id ) {
		if ( isset( $post_id ) && 'post' !== get_post_type( $post_id ) ) {
			return $post_id;
        }

		// Get all assigned terms
		$terms = get_the_terms( $post_id, 'category' );
		foreach ( $terms as $key => $term ) {
			if ( $term->parent != 0 && ! has_term( $term->parent, 'category', $post_id ) ) {
				wp_set_object_terms( $post_id, array( $term->parent ), 'category', true );
				$term = get_term( $term->parent, 'category' );
			}
		}
	}

	/**
	 * Automatically check a child category as the primary category
     *
	 * @param string $post_id ID of the post.
	 */
	public function assign_child_category_as_primary( $post_id ) {
		$primary_categ = ArticleUtils::get_post_primary_category( $post_id );

		// Get assigned child categories
		$terms       = wp_get_post_terms( $post_id, 'category' );
		$child_terms = array_filter(
			$terms,
			function( $term ) {
				return 0 !== $term->parent;
			}
		);

		if ( ! $primary_categ || ( $primary_categ && ! in_array( $primary_categ, $child_terms ) ) ) {
			// Only set primary if there is only 1 child assigned
			if ( count( $child_terms ) === 1 ) {
				$primary_child = array_values( $child_terms )[0];
				update_post_meta( $post_id, '_yoast_wpseo_primary_category', $primary_child->term_id );
			}
		}
	}
}