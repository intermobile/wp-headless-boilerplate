<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Hooks for customizations to the Gutenberg editor or the Classic editor
 */
class Editor extends Hook {
	public function __construct() {
		$this->add_filter( 'after_setup_theme', 'add_custom_editor_styles', 2 );
		$this->add_filter( 'after_setup_theme', 'add_headings_tooltips' );
		$this->add_action( 'current_screen', 'disable_gutenberg_per_post_type' );
	}

	/**
	 * Add custom CSS to the Gutenberg editor
	 */
	public function add_custom_editor_styles(): void {
		add_theme_support( 'editor-styles' );
		add_editor_style( 'assets/css/editor.css' );
	}

	/**
	 * Add editorial tooltips to the headings in the Gutenberg editor
	 */
	public function add_headings_tooltips(): void {
		$user_id = get_current_user_id();
		if ( function_exists( 'acf_add_local_field_group' ) && ! get_user_meta( $user_id, 'disable_heading_tooltips', true ) ) {
			add_editor_style( 'assets/css/headings.css' );
		}
	}

	/**
	 * Disable the gutenberg editor for a set of post types
	 */
	public function disable_gutenberg_per_post_type() {
		$current_screen    = get_current_screen();
		$current_post_type = $current_screen->post_type;

		// Define the post types to use the classic editor
		$use_classic_editor = array( 'page' );

		if ( in_array( $current_post_type, $use_classic_editor, true ) ) {
			$this->disable_gutenberg_editor();
		}
	}

	/**
	 * Remove Gutenberg actions and filters
	 */
	private function disable_gutenberg_editor() {
		if ( ! is_plugin_active( 'classic-editor/classic-editor.php' ) ) { // Stop disabling Gutenberg Editor if is enabled Classic Editor plugin

			$gutenberg    = function_exists( 'gutenberg_can_edit_post_type' );
			$block_editor = has_action( 'enqueue_block_assets' );
			if ( ! $gutenberg && false === $block_editor ) {
				return;
            }

			if ( $block_editor ) {
				add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );
			}

			if ( $gutenberg ) {
				add_filter( 'gutenberg_can_edit_post_type', '__return_false', 100 );

				// Synced w/ Classic Editor plugin
				remove_action( 'admin_menu', 'gutenberg_menu' );
				remove_action( 'admin_init', 'gutenberg_redirect_demo' );

				remove_filter( 'wp_refresh_nonces', 'gutenberg_add_rest_nonce_to_heartbeat_response_headers' );
				remove_filter( 'get_edit_post_link', 'gutenberg_revisions_link_to_editor' );
				remove_filter( 'wp_prepare_revision_for_js', 'gutenberg_revisions_restore' );

				remove_action( 'rest_api_init', 'gutenberg_register_rest_routes' );
				remove_action( 'rest_api_init', 'gutenberg_add_taxonomy_visibility_field' );
				remove_filter( 'rest_request_after_callbacks', 'gutenberg_filter_oembed_result' );
				remove_filter( 'registered_post_type', 'gutenberg_register_post_prepare_functions' );

				remove_action( 'do_meta_boxes', 'gutenberg_meta_box_save', 1000 );
				remove_action( 'submitpost_box', 'gutenberg_intercept_meta_box_render' );
				remove_action( 'submitpage_box', 'gutenberg_intercept_meta_box_render' );
				remove_action( 'edit_page_form', 'gutenberg_intercept_meta_box_render' );
				remove_action( 'edit_form_advanced', 'gutenberg_intercept_meta_box_render' );
				remove_filter( 'redirect_post_location', 'gutenberg_meta_box_save_redirect' );
				remove_filter( 'filter_gutenberg_meta_boxes', 'gutenberg_filter_meta_boxes' );

				remove_action( 'admin_notices', 'gutenberg_build_files_notice' );
				remove_filter( 'body_class', 'gutenberg_add_responsive_body_class' );
				remove_filter( 'admin_url', 'gutenberg_modify_add_new_button_url' ); // old
				remove_action( 'admin_enqueue_scripts', 'gutenberg_check_if_classic_needs_warning_about_blocks' );
				remove_filter( 'register_post_type_args', 'gutenberg_filter_post_type_labels' );

				remove_action( 'admin_init', 'gutenberg_add_edit_link_filters' );
				remove_action( 'admin_print_scripts-edit.php', 'gutenberg_replace_default_add_new_button' );
				remove_filter( 'redirect_post_location', 'gutenberg_redirect_to_classic_editor_when_saving_posts' );
				remove_filter( 'display_post_states', 'gutenberg_add_gutenberg_post_state' );
				remove_action( 'edit_form_top', 'gutenberg_remember_classic_editor_when_saving_posts' );
			}
		}
	}
}