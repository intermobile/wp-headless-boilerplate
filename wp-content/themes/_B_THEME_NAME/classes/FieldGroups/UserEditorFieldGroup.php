<?php

namespace Theme\FieldGroups;

use Solidify\Core\FieldGroup;
use Solidify\Fields;

class UserEditorFieldGroup extends FieldGroup { // phpcs:ignore
    public function __construct() { // phpcs:ignore
        $this->set_fields(
            array(
				'disable_heading_tooltips' => new Fields\TrueFalse(
					'Heading Tooltips',
					array(
						'message'       => '<b>Disable tooltips for headings in the editor</b>',
						'instructions'  => 'By default, the editor shows tooltips for headings like <code>&lt;h2&gt;</code>, <code>&lt;h3&gt;</code>, <code>&lt;h4&gt;</code> and so on.',
						'ui'            => 0,
						'default_value' => 0,
					)
				),
			)
        );

        $this->args = array(
			'key'                   => 'editor-options',
			'title'                 => 'Editor Options',
			'menu_order'            => 1,
			'instruction_placement' => 'field',
			'location'              => array(
				array(
					array(
						'param'    => 'user_role',
						'operator' => '==',
						'value'    => 'all',
					),
				),
			),
        );
    }
}