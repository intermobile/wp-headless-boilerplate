<?php

namespace Theme\FieldGroups;

use Solidify\Core\FieldGroup;
use Solidify\Core\OptionsPage;
use Solidify\Fields;
use Theme\CustomFields\WebsiteFooterFields;
use Theme\CustomFields\WebsiteHeaderFields;
use Theme\CustomFields\WebsiteGeneralFields;

class WebsiteSettingsFieldGroup extends FieldGroup { // phpcs:ignore
	public function __construct() { // phpcs:ignore
		$general = new WebsiteGeneralFields();
		$header  = new WebsiteHeaderFields();
		$footer  = new WebsiteFooterFields();

		$this->set_fields(
			array(
				'general_tab'        => new Fields\Tab( 'General' ),
				$general->fields,
				'website_header_tab' => new Fields\Tab( 'Header' ),
				$header->fields,
				'footer_tab'         => new Fields\Tab( 'Footer' ),
				$footer->fields,
			)
		);

		$this->args = array(
			'key'      => 'website-settings',
			'title'    => 'Website Settings',
			'layout'   => 'seamless',
			'location' => array(
				array(
					OptionsPage::is_equal_to( 'website-options' ),
				),
			),
		);
	}
}
