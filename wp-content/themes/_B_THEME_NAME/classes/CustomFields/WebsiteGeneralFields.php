<?php

namespace Theme\CustomFields;

use Solidify\Core\CustomField;
use Solidify\Fields;

class WebsiteGeneralFields extends CustomField { // phpcs:ignore
	public function __construct() { // phpcs:ignore
		$this->fields = array(
			'website_url' => new Fields\URL(
				'Website URL <code>Front-end</code>',
				array(
					'instructions' => 'This is used for some links in the CMS, configure the <b>Preview</b> buttons, and more.',
					'wrapper'      => array(
						'width' => '50',
					),
				)
			),
		);
	}
}