<?php

namespace Theme\CustomFields;

use Solidify\Core\CustomField;
use Solidify\Fields;

class WebsiteFooterFields extends CustomField { // phpcs:ignore
	public function __construct() { // phpcs:ignore
		$this->fields = array(
			'footer-copyright_text' => new Fields\Text(
				'Copyright',
				array(
					'instructions' => 'Define the footer copyright text. Use <code><em>[%YEAR%]</em></code> as a placeholder to the current year.',
				)
			),
			'footer-message_text'   => new Fields\Message(
				'Menus',
				array(
					'message' => 'Footer links and social networks are managed in the <code>Appearance</code> > <code>Menus</code> panel.',
				)
            ),
		);
	}
}