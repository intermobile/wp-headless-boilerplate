<?php

namespace Theme\CustomFields;

use Solidify\Core\CustomField;
use Solidify\Fields;

class WebsiteHeaderFields extends CustomField { // phpcs:ignore
	public function __construct() { // phpcs:ignore
		$this->fields = array(
			'header-message_text' => new Fields\Message(
				'Menus',
				array(
					'message' => 'Header and mobile navigation menus are managed in the <code>Appearance</code> > <code>Menus</code> panel.',
				)
            ),
		);
	}
}