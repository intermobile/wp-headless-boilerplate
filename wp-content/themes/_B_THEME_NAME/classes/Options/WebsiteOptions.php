<?php

namespace Theme\Options;

use Solidify\Core\OptionsPage;

class WebsiteOptions extends OptionsPage {
	public function __construct() {
		$this->args = array(
			'page_title' => 'Website Settings',
			'menu_title' => 'Website',
			'menu_slug'  => 'website-options',
			'capability' => 'edit_posts',
			'redirect'   => false,
		);
	}
}
