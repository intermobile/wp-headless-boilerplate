<?php

namespace Theme\Helpers;

/**
 * PageUtils class
 *
 * A collection of utility functions to handle data from the page post type
 *
 * @package Theme\Helpers
 */
class PageUtils {
	/**
	 * Returns the filtered layout name, based on the page template
	 *
	 * @param int $page_id ID of the page.
	 */
	public static function get_page_layout( $page_id ) {
		$page_template = get_page_template_slug( $page_id );
		if ( $page_template ) {
			$exploded = explode( '/', $page_template );
			return str_replace( array( 'template-', '.php' ), '', $exploded[ count( $exploded ) - 1 ] );
		} elseif ( intval( get_option( 'page_on_front' ) ) === $page_id ) {
			return 'home';
		} else {
			return 'default';
		}
	}

	/**
     * Returns an array with all parent pages from a page
	 *
     * @param int $page_id ID from the page.
     * @return WP_Post[]
     */
    public static function get_page_ascendancy( $page_id ) {
		$pages = array();
		$page  = get_post( $page_id );
        if ( $page ) {
			while ( $page->post_parent > 0 ) {
				$page    = get_post( $page->post_parent );
				$pages[] = $page;
			}
        }
        return $pages;
    }

	/**
     * Returns array of breadcrumbs of a page.
     *
     * @param int $page_id ID of the post.
     * @return object Object with 'name' and 'path' properties
     */
	public static function get_page_breadcrumbs( $page_id ) {
		// Get parent pages
		$pages = self::get_page_ascendancy( $page_id );

		// Build breadcrumb objects
		$breadcrumbs = array();
		foreach ( array_reverse( $pages ) as $page ) {
			$breadcrumbs[] = (object) array(
				'title' => $page->post_title,
				'path'  => ThemeUtils::get_relative_path( get_the_permalink( $page->ID ) ),
			);
		}
        return $breadcrumbs;
    }
}
