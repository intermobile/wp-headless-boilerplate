<?php

namespace Theme\Helpers;

/**
 * Utils class
 *
 * A collection of utility functions to be used between classes and templates
 *
 * @package Theme\Helpers
 */
class ThemeUtils {

	/**
	 * Prints the variable, object or array content with a <pre> tag and print_r()
     *
	 * @param any $to_print Variable with content to be printed.
	 */
	public static function showme( $to_print ) {
		echo '<pre class="showme">' . print_r( $to_print, true ) . '</pre>';
	}

	/**
	 * Returns true if on production environment
     *
	 * @return bool Whether ENV is set as 'production' or not
	 */
	public static function is_production(): bool {
		if ( ! defined( 'ENV' ) ) {
			define( 'ENV', 'local' );
		}
		return ENV === 'production';
	}

	/**
	 * Return the slug of the current market blog site
	 */
	public static function get_market_slug() {
		switch ( get_current_blog_id() ) {
			case 2:
				return 'en-us';
			case 3:
				return 'en-uk';
			default:
				return '';
		}
	}

	/**
	 * Check if the current blog is one of the tested markets
     *
	 * @param string|array $market_slug The market slug or an array of market slugs.
	 */
	public static function is_market( $market_slug ) {
		$is_market = false;

		// Check if it's an array instead of single string
		if ( is_array( $market_slug ) ) {
			foreach ( $market_slug as $slug ) {
				if ( self::get_market_slug() == $slug ) {
					$is_market = true;
				}
			}
		} else { // Is single string
			$is_market = self::get_market_slug() === $market_slug;
		}
		return $is_market;
	}

	/**
	 * Returns true if on stage environment
     *
	 * @return bool Whether ENV is set as 'stage' or not
	 */
	public static function is_stage(): bool {
		if ( ! defined( 'ENV' ) ) {
			define( 'ENV', 'local' );
		}
		return ENV === 'stage';
	}

	/**
	 * Returns true if on homolog environment
     *
	 * @return bool Whether ENV is set as 'homolog' or not
	 */
	public static function is_homolog(): bool {
		if ( ! defined( 'ENV' ) ) {
			define( 'ENV', 'local' );
		}
		return ENV === 'homolog';
	}

    /**
     * Return a relative url path
     *
     * @param string $url Full URL.
     * @return string
     */
    public static function get_relative_path( $url ) {
		$website_url = untrailingslashit( get_option( 'options_website_url', '' ) );
        return str_replace( array( get_home_url( null ), './', $website_url ), '', $url );
    }

	/**
	 * Get template file passing args keys as variables to the included file
     *
	 * @param string $file Relative path of the file from theme root (without extension).
	 * @param array  $args Associative array to parse into variables.
	 */
	public static function get_template( $file, $args = array() ) {
		if ( $args && is_array( $args ) ) {
			extract( $args );
		}

		$filepath = get_template_directory() . '/' . $file . '.php';
		if ( ! file_exists( $filepath ) ) {
			return;
		}

		include $filepath;
	}

	/**
	 * Returns a string with the CSS file content
     *
	 * @param string $file CSS file path (without extension).
	 * @param array  $vars Variables to replace in the CSS file.
	 *
	 * @return array $vars Optional array with variables to be replaced. i.e. array( "heading-color" => "red" )
	 */
	public static function get_style_file_content( $file, $vars = array() ) {
		$file_path    = get_template_directory() . '/' . $file . '.css';
		$file_content = file_get_contents( $file_path );
		foreach ( $vars as $var => $value ) {
			$file_content = str_replace( ( '--' . $var ), $value, $file_content );
		}
		return $file_content;
	}

	/**
	 * Get meta description data (Yoast SEO) from one or more objects
	 *
	 * @param int|array $object_id ID of single object, or array of IDs.
	 * @param string    $obj_type Type of the object (i.e. post, term, user).
	 * @param string    $obj_sub_type Specific type of object (i.e. post, page, category, tag).
	 */
	public static function get_seo_meta_description( $object_id, $obj_type, $obj_sub_type = null ) {
		global $wpdb;
		$query  = "SELECT object_id FROM {$wpdb->prefix}yoast_indexable ";
		$query .= "WHERE object_type = '$obj_type' AND LENGTH(description) > 0 ";
		if ( $obj_sub_type ) {
			$query .= "AND object_sub_type = '$obj_sub_type' ";
		}
		if ( is_array( $object_id ) ) {
			$query .= 'AND object_id IN(' . implode( ',', $object_id ) . ')';
		} else {
			$query .= "AND object_id = '$object_id'";
		}
		$results    = $wpdb->get_results( $query );
		$result_ids = array();
		foreach ( $results as $row ) {
			$result_ids[] = $row->object_id;
		}
		return $result_ids;
	}
}