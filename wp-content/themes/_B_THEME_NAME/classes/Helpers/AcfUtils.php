<?php

namespace Theme\Helpers;

/**
 * AcfUtils class
 *
 * A collection of functions for handling ACF data
 *
 * @package Theme\Helpers
 */
class AcfUtils {
	/**
	 * Returns all the ACF data from a specific route (context) formatted
	 *
	 * @param string $context Type of context where to get data from, i.e. 'post', 'category', 'options', custom taxonomies, etc. Default is 'post'.
	 * @param int    $object_id The object ID (page, post, category, etc.) or other context like 'options'.
	 */
	public static function get_formatted_fields( $context = 'post', $object_id = null ): array {
		$acf_context = '';
		switch ( $context ) {
			case 'post':
			case 'page':
				$acf_context = $object_id;
				break;
			case 'options':
				$acf_context = 'options';
				break;
			default:
				$acf_context = "{$context}_{$object_id}";
				break;
		}

		// Get all fields from context
		$fields = get_fields( $acf_context );

		// Group the fields using the separator character to build the hierarchy
		$separator      = '-';
		$grouped_fields = array();
		if ( $fields ) {
			foreach ( $fields as $key => $value ) {
				$field_object = get_field_object( $key, $acf_context );
				$exploded_key = explode( $separator, $key );
				switch ( count( $exploded_key ) ) {
					case 1:
						$grouped_fields[ $exploded_key[0] ] = self::get_formatted_field( $field_object, $value );
						break;
					case 2:
						$grouped_fields[ $exploded_key[0] ][ $exploded_key[1] ] = self::get_formatted_field( $field_object, $value );
						break;
					case 3:
						$grouped_fields[ $exploded_key[0] ][ $exploded_key[1] ][ $exploded_key[2] ] = self::get_formatted_field( $field_object, $value );
						break;
					default:
						// Does not support more than 3 levels
						break;
				}
			}
		}

		return $grouped_fields;
	}

	/**
	 * Returns data from a post type
	 *
	 * @param int $post_id ID of the post.
	 */
	private static function format_post_type( $post_id ): array {
		$post_type = get_post_type( $post_id );

		switch ( $post_type ) {
			case 'post':
				return new ArticleObject( $post_id );
			default:
				return self::get_formatted_fields( 'post', $post_id );
		}
	}

	/**
	 * Returns data from a media
	 *
	 * @param int $image_id ID of the image.
	 */
	private static function format_image( $image_id ): ?ImageObject {
		return $image_id ? new ImageObject( $image_id ) : null;
	}

	/**
	 * Returns data from a text field, doing some replaces
	 *
	 * @param string $text Text to be handled.
	 */
	private static function format_text( $text ): string {
		return str_replace(
			array(
				'[%YEAR%]',
				'[%SITE_NAME%]',
			),
			array(
				gmdate( 'Y' ),
				get_bloginfo( 'name' ),
			),
            $text
        );
	}

	/**
	 * Returns data from a WYSIWYG editor field
	 *
	 * @param string $content Text to be handled.
	 */
	private static function format_wysiwyg( $content ): string {

		// Replace URLs in the content by relative URLs
		$matches = preg_match_all( '/http:.*?(?=[\'"])/', $content, $matched_urls );
		if ( $matches ) {
			foreach ( $matched_urls[0] as $url ) {
				$file_url_pattern = '/\.((jpg)|(jpeg)|(png)|(webp)|(gif)|(svg)|(pdf)|(wav)|(mp3)|(mp4)|(mpg)|(mov)|(wmv)|(doc)|(xls)|(ppt))$/';
				if ( ! preg_match( $file_url_pattern, $url ) ) {
					$new_url = ThemeUtils::get_relative_path( $url );
					$content = str_replace( $url, $new_url, $content );
				}
			}
		}
		return $content;
	}

	/**
	 * Returns data from a link field, converting self URLs for relative
	 *
	 * @param array $link Array with title, url and target properties.
	 */
	private static function format_link( $link ): ?array {
		if ( $link ) {
			$file_url_pattern = '/\.((jpg)|(jpeg)|(png)|(webp)|(gif)|(svg)|(pdf)|(wav)|(mp3)|(mp4)|(mpg)|(mov)|(wmv)|(doc)|(xls)|(ppt))$/';
			$link['url']      = ! preg_match( $file_url_pattern, trim( $link['url'] ) ) ? trim( ThemeUtils::get_relative_path( $link['url'] ) ) : trim( $link['url'] );
			return $link;
		} else {
			return null;
		}
	}

	/**
	 * Returns data from a ACF group field.
	 *
	 * @param array $field_object ACF field object.
	 * @param array $data Value from the field.
	 */
	private static function format_group( $field_object, $data ): ?array {
		if ( ! $data ) {
			return null;
		}

		$return_data = array();
		foreach ( $data as $key => $value ) {
			foreach ( $value as $data_key => $data_value ) {
				foreach ( $field_object['sub_fields'] as $field_key => $field_value ) {
					if ( $field_value['name'] == $data_key ) {
						$return_data[ $key ][ $data_key ] = self::get_formatted_field( $field_object['sub_fields'][ $field_key ], $data_value );
					}
				}
			}
		}

		return $return_data;
	}

	/**
	 * Returns data from a ACF group field.
	 *
	 * @param array $field_object ACF field object.
	 * @param array $data Value from the field.
	 */
	private static function format_flexible( $field_object, $data ): ?array {
		if ( ! $data ) {
			return null;
		}

		$return_data = array();
		foreach ( $data as $key => $value ) {
			foreach ( $value as $data_key => $data_value ) {
				foreach ( $field_object['layouts'] as $layouts_key => $layouts_value ) {
					foreach ( $layouts_value['sub_fields'] as $field_key => $field_value ) {
						if ( $field_value['name'] == $data_key ) {
							$return_data[ $key ]['acf_layout'] = $value['acf_fc_layout'];
							$return_data[ $key ][ $data_key ]  = self::get_formatted_field( $layouts_value['sub_fields'][ $field_key ], $data_value );
						}
					}
				}
			}
		}

		return $return_data;
	}

	/**
	 * Returns formatted data from an ACF field, making adjutments to the returned content
	 *
	 * @param array $field_object ACF field object.
	 * @param array $value Value from the field.
	 */
	private static function get_formatted_field( $field_object, $value ) {
		switch ( $field_object['type'] ) {
			case 'text':
				$formatted_value = self::format_text( $value );
				break;
			case 'post_object':
				$formatted_value = self::format_post_type( $value );
				break;
			case 'image':
				if ( ! ThemeUtils::is_production() && is_array( $value ) ) {
					trigger_error( "The {$field_object['name']} field doesn't have the expected data type. Make sure the field is configured to return the id instead of array (default) with 'return_format' => 'id'", E_USER_WARNING );
				}
				$formatted_value = self::format_image( is_array( $value ) ? $value['ID'] : $value );
				break;
			case 'link':
				$formatted_value = self::format_link( $value );
				break;
			case 'wysiwyg':
				$formatted_value = self::format_wysiwyg( $value );
				break;
			case 'repeater':
			case 'group':
				$formatted_value = self::format_group( $field_object, $value );
				break;
			case 'flexible_content':
				$formatted_value = self::format_flexible( $field_object, $value );
				break;
			default:
				$formatted_value = $value;
				break;
		}

		return $formatted_value;
	}
}
