<?php

namespace Theme\Helpers;

/**
 * ImageSizeObject class
 *
 * A common structure for data of an image size variation.
 *
 * @package Theme\Helpers
 */
class ImageSizeObject {

	/**
	 * URI of the image file
	 *
	 * @var string
	 */
	public $url;

	/**
	 * Image original width size
	 *
	 * @var int
	 */
	public $width;

	/**
	 * Image original height size
	 *
	 * @var int
	 */
	public $height;

	/**
	 * A class for storing image size data
	 *
	 * @param string $image_url URL of the image file.
	 * @param int    $image_width Width of the image.
	 * @param int    $image_height Height of the image.
	 */
	public function __construct( $image_url, $image_width, $image_height ) {
		$this->url    = $image_url;
		$this->width  = $image_width;
		$this->height = $image_height;
	}
}
