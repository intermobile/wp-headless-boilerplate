<?php

namespace Theme\Helpers;

/**
 * TaxonomyUtils class
 *
 * A collection of functions for managing taxonomies (categories, tags, etc.) data
 *
 * @package Theme\Helpers
 */
class TaxonomyUtils {
	/**
     * Returns an array with all parent terms from a term
	 *
     * @param int    $term_id ID from the term.
	 * @param string $taxonomy Name of the taxonomy.
     * @return WP_Term[]
     */
    public static function get_term_ascendancy( $term_id, $taxonomy ) {
		$terms = array();
		$term  = get_term( $term_id, $taxonomy );
        if ( $term ) {
			while ( $term->parent > 0 ) {
				$term    = get_term( $term->parent, $taxonomy );
				$terms[] = $term;
			}
        }
        return $terms;
    }

	/**
     * Returns array of breadcrumbs of a term from a taxonomy.
     *
     * @param int    $term_id ID of the category.
	 * @param string $taxonomy Name of the taxonomy.
     * @return object Object with 'name' and 'path' properties
     */
    public static function get_term_breadcrumbs( $term_id, $taxonomy ) {
		// Get parent terms
		$terms = self::get_term_ascendancy( $term_id, $taxonomy );

		// Build breadcrumb objects
		$breadcrumbs = array();
		foreach ( array_reverse( $terms ) as $term ) {
			$breadcrumbs[] = (object) array(
				'name' => $term->name,
				'path' => ThemeUtils::get_relative_path( get_term_link( $term, $taxonomy ) ),
			);
		}
		return $breadcrumbs;
    }

	/**
     * Returns array of breadcrumbs from categories and subcategories.
     *
     * @param int $category_id ID of the category.
     * @return object Object with 'name' and 'path' properties
     */
    public static function get_category_breadcrumbs( $category_id ) {
		return self::get_term_breadcrumbs( $category_id, 'category' );
    }

	/**
     * Returns the child terms from a parent term.
     *
     * @param int    $term_id ID from the term.
	 * @param string $taxonomy Name of the taxonomy.
     * @return WP_Term[]
     */
	public static function get_child_terms( $term_id, $taxonomy ) {
		$term = get_term( $term_id, $taxonomy );

		if ( ! $term ) {
			return array();
		}

		// Get children terms
        $children = get_terms(
            array(
				'taxonomy' => $taxonomy,
				'parent'   => $term_id,
            )
        );

        return $children;
    }
}
