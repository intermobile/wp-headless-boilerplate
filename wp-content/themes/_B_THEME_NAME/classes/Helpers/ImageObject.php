<?php

namespace Theme\Helpers;

/**
 * ImageObject class
 *
 * A common structure for image data.
 *
 * @package Theme\Helpers
 */
class ImageObject {

	/**
	 * Media ID
	 *
	 * @var int
	 */
	public $id;

	/**
	 * Name of the file
	 *
	 * @var string
	 */
	public $filename;

	/**
	 * URI of the image file
	 *
	 * @var string
	 */
	public $url;

	/**
	 * Image original width size
	 *
	 * @var int
	 */
	public $width;

	/**
	 * Image original height size
	 *
	 * @var int
	 */
	public $height;

	/**
	 * Alternative text that describes the image content
	 *
	 * @var string
	 */
	public $alt;

	/**
	 * File extension type, usually jpeg, jpg, png or gif
	 *
	 * @var string
	 */
	public $type;

	/**
	 * Array of ImageSizeObject
	 *
	 * @var array
	 */
	public $sizes;

	/**
	 * A class for storing image data
	 *
	 * @param string $media_id ID of the image to get data from.
	 */
	public function __construct( $media_id = null ) {

		if ( null !== $media_id ) {
			$this->id = $media_id;

			// Get basic info
			$image_meta   = wp_get_attachment_metadata( $this->id );
			$this->width  = $image_meta['width'];
			$this->height = $image_meta['height'];
			$this->url    = wp_get_attachment_url( $this->id );

			// Get additional info
			$exploded_url   = explode( '/', $this->url );
			$this->filename = end( $exploded_url );
			$this->type     = explode( '/', get_post_mime_type( $this->id ) )[1];
			$this->alt      = $this->get_image_alt_text( $this->id, $this->format_filename_for_alt( $this->filename ) );
			$this->sizes    = $this->get_image_sizes( $image_meta['sizes'], $this->url );
		}
	}

	/**
	 * Returns the data of the image size, if not found returns the original image
	 *
	 * @param ImageObject $image_object Image data as an Image.
	 * @param string      $size Label of the size needed.
	 * @return ImageSizeObject Image size data
	 */
	public static function get_size( $image_object, $size = '' ) {
		if ( $image_object instanceof ImageObject ) {
			if ( array_key_exists( $size, $image_object->sizes ) ) {
				// Return the requested size data
				return $image_object->sizes[ $size ];
			} else {
				// If size doens't exist, return data from the original size
				return new ImageSizeObject( $image_object->url, $image_object->width, $image_object->height );
			}
		}
	}

	/**
	 * Get a list of ImageSizeObject using the image sizes meta data
	 *
	 * @param array  $sizes_meta Sizes meta data from wp_get_attachment_metadata().
	 * @param string $image_url URI of the image file.
	 * @return array List of ImageSizeObject(s)
	 */
	private static function get_image_sizes( $sizes_meta = array(), $image_url = '' ) {
		// Get folder URI from original image URL
		$exploded = explode( '/', $image_url );
		array_pop( $exploded );
		$image_path = implode( '/', $exploded );

		// Build the array of sizes
		$sizes = array();
		foreach ( $sizes_meta as $key => $size ) {
			$sizes[ $key ] = new ImageSizeObject( "{$image_path}/{$size['file']}", $size['width'], $size['height'] );
		}

		return $sizes;
	}

	/**
	 * Receive the image filename and format to use as alt text
	 *
	 * @param string $filename Name of the image file.
	 * @return string Formatted text
	 */
	private function format_filename_for_alt( $filename = '' ) {
		$replace = array(
			'from' => array( '/\.\w+$/', '/[-_]/' ),
			'to'   => array( '', ' ' ),
		);
		return ucfirst( preg_replace( $replace['from'], $replace['to'], $filename ) );
	}

	/**
	 * Get the alternative text of an image
	 *
	 * @param int    $image_id ID of the media in the database.
	 * @param string $fallback A fallback text to use in case the image doesn't have an alt text registered. Usually, the image title or file name.
	 * @return string The alt text
	 */
	private function get_image_alt_text( $image_id = null, $fallback = '' ) {
		$alt_meta = get_post_meta( $image_id, '_wp_attachment_image_alt' );

		// Check if the ALT meta is defined. If not, use the fallback text instead
		$alt_text = '';
		if ( ! empty( $alt_meta ) && $alt_meta[0] ) {
			$alt_text = $alt_meta[0];
		} else {
			$alt_text = $fallback;
		}

		return $alt_text;
	}

	/**
	 * Gets the URI from a image on the theme assets folder.
     *
	 * @param string $file_name Image file name with extension.
	 * @return string
	 */
	public static function get_image_uri_from_assets( $file_name ) {
		return get_template_directory_uri() . "/assets/img/{$file_name}";
	}
}
