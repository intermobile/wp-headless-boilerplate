<?php

namespace Theme\Helpers;

/**
 * Util functions for enqueues
 *
 * @package Theme\Helpers
 */
class Enqueues {
	/**
	 * Return the path of a CSS file from the /assets/css folder
     *
	 * @param string $filename Name of the CSS file, without the extenstion.
	 */
	public static function get_css_path( $filename ) {
		return get_template_directory() . "/assets/css/{$filename}.css";
	}

	/**
	 * Return the URI of a CSS file from the /assets/css folder
     *
	 * @param string $filename Name of the CSS file, without the extenstion.
	 */
	public static function get_css_uri( $filename ) {
		return get_template_directory_uri() . "/assets/css/{$filename}.css";
	}
}
