<?php

namespace Theme\Helpers;

/**
 * CategoryUtils class
 *
 * A collection of functions for managing taxonomies category data
 *
 * @package Theme\Helpers
 */
class CategoryUtils {
	/**
     * Returns the child categories from a parent category.
     *
     * @param int $category_id ID from the category.
     * @return WP_Term[]
     */
	public static function get_child_categories( $category_id ) {
		return TaxonomyUtils::get_child_terms( $category_id, 'category' );
    }

	/**
     * Returns array of breadcrumbs from categories and subcategories.
     *
     * @param int $category_id ID of the category.
     * @return object Object with 'name' and 'path' properties
     */
    public static function get_category_breadcrumbs( $category_id ) {
		return TaxonomyUtils::get_term_breadcrumbs( $category_id, 'category' );
    }
}