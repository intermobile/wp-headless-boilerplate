<?php

namespace Theme\Helpers;

use Theme\Helpers\ThemeUtils;

/**
 * TaxonomyObject class
 *
 * A common structure for taxonomy (categories and tags) data
 *
 * @package Theme\Helpers
 */
class TaxonomyObject {

	/**
	 * Term ID
	 *
	 * @var int
	 */
	public $id;

	/**
	 * Name of the taxonomy
	 *
	 * @var string
	 */
	public $name;

	/**
	 * Slug of the taxonomy
	 *
	 * @var string
	 */
	public $slug;

	/**
	 * Relative URL of the taxonomy
	 *
	 * @var string
	 */
	public $path;

	/**
	 * Term ID of the parent taxonomy, 0 if has no parent
	 *
	 * @var int
	 */
	public $parent;

	/**
	 * A class for storing taxonomy data
	 *
	 * @param WP_Term $term Object of the taxonomy.
	 */
	public function __construct( $term = null ) {
		if ( $term instanceof \WP_Term ) {
			$this->id     = $term->term_id;
			$this->name   = $term->name;
			$this->slug   = $term->slug;
			$this->path   = ThemeUtils::get_relative_path( get_term_link( $term->term_id, $term->taxonomy ) );
			$this->parent = $term->parent;
		}
	}

	/**
	 * Return an TaxonomyObject array from received WP_Term array
	 *
	 * @param array $terms Array of taxonomies as WP_Term object format.
	 * @return TaxonomyObject[] Array of TaxonomyObject
     */
	public static function format_taxonomies_data( $terms = array() ) {
		$taxonomy_objects = array();
		foreach ( $terms as $term ) {
			$taxonomy_objects[] = new TaxonomyObject( $term );
		}
		return $taxonomy_objects;
	}

	/**
	 * Extract the IDs from a list of TaxonomyObjects
	 *
	 * @param TaxonomyObject[] $taxonomy_objects List of object of type TaxonomyObjects.
	 * @return array List of taxonomies IDs
	 */
	public static function get_taxonomies_ids( $taxonomy_objects = array() ) {
		$ids = array();
		foreach ( $taxonomy_objects as $taxonomy ) {
			$ids[] = $taxonomy->id;
		}
		return $ids;
	}

}