<?php

namespace Theme\Helpers;

/**
 * Class TestObject for conformance tests
 *
 * @package Theme\Helpers
 */
class TestObject {

	/**
	 * Current type of object
     *
	 * @var string
	 */
	public $context = '';

	/**
	 * Flag to notify the builder if it is mandatory or not.
	 *
	 * @var bool
	 */
	public $is_mandatory = true;

	/**
	 * Frontend description.
	 *
	 * @var string
	 */
	public $description = '';

	/**
	 * Frontend message.
	 *
	 * @var string
	 */
	public $message = '';

	/**
	 * Array of test objects.
     *
	 * @var array
	 */
	public $results = array();

	/**
	 * Include a simple object as test result
	 *
	 * @param int    $id        Object ID.
	 * @param string $title     Object title.
	 * @param string $edit_link Absolute URL to edit the object in CMS.
	 */
	public function add_result( int $id, string $title, string $edit_link ) {
		$this->results[] = (object) array(
			'id'        => $id,
			'title'     => $title,
			'edit_link' => $edit_link,
		);
	}
}