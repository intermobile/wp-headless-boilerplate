<?php

namespace Theme\Helpers;

/**
 * ArticleQuery class
 *
 * A collection of functions for getting articles from database
 *
 * @package Theme\Helpers
 */
class ArticleQuery {
	/**
	 * Get a defined number os articles
	 *
	 * @param int   $quantity Number of articles to gather. Default is 12.
	 * @param array $args Additional args for the posts query.
	 * @return ArticleObject[] List of articles data.
	 */
	public static function get_articles( $quantity = 12, $args = array() ) {
		$query_args = array(
			'numberposts' => $quantity,
		);

		// Adds (or overrides) additional args
		foreach ( $args as $key => $value ) {
			$query_args[ $key ] = $value;
		}

		// Make the query
		$posts = get_posts( $query_args );

		return ArticleObject::format_articles_data( $posts );
	}

	/**
	 * Get a defined number of articles randomly
	 *
	 * @param int   $quantity Number of articles to gather. Default is 12.
	 * @param array $args Additional args for the posts query.
	 * @return ArticleObject[] List of articles data.
	 */
	public static function get_random_articles( $quantity = 12, $args = array() ) {
		return self::get_articles(
            $quantity,
            array_merge( $args, array( 'orderby' => 'rand' ) )
        );
	}

	/**
	 * Get a defined number of articles randomly
	 *
	 * @param int       $quantity Number of articles to gather. Default is 12.
	 * @param int|int[] $category_id ID of the category, or array of IDs.
	 * @param array     $args Additional args for the posts query.
	 * @return ArticleObject[] List of articles data.
	 */
	public static function get_articles_by_category( $category_id, $quantity = 12, $args = array() ) {
		return self::get_articles(
            $quantity,
            array_merge( $args, array( 'category' => is_array( $category_id ) ? implode( ',', $category_id ) : $category_id ) )
        );
	}

	/**
	 * Returns articles from the same subcategory or subcategory.
	 *
	 * @param int   $category_id ID of the post category.
     * @param int   $quantity limit quantity.
	 * @param array $exclude A list of post IDs to exclude from the query.
     * @param bool  $try_parent try to fill with parent category posts.
	 * @return ArticleObject[] List of articles data.
	 */
	public static function get_related_articles( $category_id, $quantity = 6, $exclude = array(), $try_parent = true ) {
		$articles = self::get_random_articles(
            $quantity,
			array(
				'category' => $category_id,
				'exclude'  => $exclude,
            )
		);

		// If not enough, tries to fill with parent category posts
		if ( count( $articles ) < $quantity && $try_parent ) {
            $parent_categ_articles = array();
            // Diff in qty and excluding already filled posts
            foreach ( $articles as $post ) {
                $quantity--;
                $exclude[] = $post->id;
            }

            // Detecting parent category
            $child_category = get_category( $category_id );
            if ( isset( $child_category->parent ) && $child_category->parent > 0 ) {
                // Single recursive call (do not try with parent)
                $parent_categ_articles = self::get_related_articles( $child_category->parent, $exclude, $quantity, false );
            }
            $articles = array_merge( $articles, $parent_categ_articles );
		}

		return $articles;
	}
}