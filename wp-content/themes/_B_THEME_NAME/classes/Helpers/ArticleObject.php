<?php

namespace Theme\Helpers;

use Theme\Helpers\ThemeUtils;
use Theme\Helpers\ArticleUtils;
use \WP_Post;

/**
 * ArticleObject class
 *
 * A common structure for article data encapsulating  the WP_Post class
 *
 * @package Theme\Helpers
 */
class ArticleObject {

	/**
     * ID of the post
     *
     * @var int
     */
    public $id;

    /**
     * Title of the post
     *
     * @var string
     */
    public $title;

	/**
     * Slug of the post
     *
     * @var string
     */
    public $slug;

	/**
     * Relative URL of the post page
     *
     * @var string
     */
    public $path;

	/**
	 * Article thumbnail image data.
	 *
	 * @var string
	 */
	public $image;

	/**
	 * ID of the primary category
	 *
	 * @var int
	 */
	public $primary_category;

	/**
	 * ID of the assigned categories
	 *
	 * @var int
	 */
	public $categories;

	/**
	 * Formatted publishing date.
	 *
	 * @var string
	 */
	public $formatted_date;

	/**
	 * Estimated reading time (in minutes) of the article by content size.
	 *
	 * @var int
	 */
	public $estimated_reading_time;

    /**
	 * Excerpt of the article.
	 *
     * @var string
     */
	public $excerpt;

    /**
     * URL of the post page
     *
     * @var string
     */
    protected $permalink;

    /**
     * Publishing time in standard format.
     *
     * @var string
     */
    protected $publish_time;

    /**
     * Original WP_Post data.
     *
     * @var object
     */
    protected $post_data;

    /**
	 * A class for storing article data
	 *
	 * @param string $post (Optional) Id of the post to get data from. If not provided will try to get post from current context.
	 */
	public function __construct( $post = null ) {
		// inheritance of PostObject
        if ( is_int( $post ) ) {
            // If $post is an ID, get the post data object
            $this->post_data = get_post( $post );
        } elseif ( $post instanceof WP_Post ) {
            // Get data from WP_Post object
            $this->post_data = $post;
        } else {
			// As $post was not provided, get data from current context
            $this->post_data = get_post( get_the_ID() );
		}

		if ( ! $this->post_data instanceof WP_Post ) {
			return null;
		}

		// Get basic data from WP_Post object to this object
		$this->title = $this->post_data->post_title;
		$this->id    = $this->post_data->ID;
		$this->slug  = $this->post_data->post_name;
		// non-public values
		$this->permalink    = get_permalink( $this->post_data->ID );
		$this->publish_time = $this->post_data->post_date;

		// Get featured image
		if ( has_post_thumbnail( $this->id ) ) {
			$thumbnail_id = get_post_thumbnail_id( $this->id );
			$this->image  = new ImageObject( $thumbnail_id );
		}

		// Categories IDs
		$primary_category = ArticleUtils::get_post_primary_category( $this->id );
		$categories_ids   = array_map(
            function( $category ) {
				return $category->term_id;
			},
            get_the_category( $this->id )
        );

		// Get data for article card
        $this->path                   = ThemeUtils::get_relative_path( $this->permalink );
		$this->primary_category       = $primary_category ? $primary_category->term_id : null;
		$this->categories             = $categories_ids;
        $this->formatted_date         = get_the_date( get_option( 'date_format' ), $this->id );
        $this->estimated_reading_time = ArticleUtils::get_post_reading_time( $this->post_data->post_content );
        $this->excerpt                = get_the_excerpt( $this->id );
	}

	/**
	 * Return an ArticleObject array from received WP_Post array
	 *
	 * @param array $posts Array of posts as WP_Post object format.
	 *
	 * @return ArticleObject[] Array of ArticleObject
     */
	public static function format_articles_data( $posts = array() ) {
		$article_objects = array();
		foreach ( $posts as $post ) {
			$article_objects[] = new ArticleObject( $post );
		}
		return $article_objects;
	}

	/**
	 * Extract the IDs from a list of ArticleObjects
	 *
	 * @param ArticleObject[] $article_objects List of object of type ArticleObjects.
	 *
	 * @return array List of IDs
	 */
	public static function get_articles_ids( $article_objects = array() ) {
		$ids = array();
		foreach ( $article_objects as $article ) {
			if ( isset( $article->id ) ) {
                $ids[] = $article->id;
            }
		}
		return $ids;
	}
}
