<?php

namespace Theme\Helpers;

/**
 * ArticleUtils class
 *
 * A collection of functions for handling with post data
 *
 * @package Theme\Helpers
 */
class ArticleUtils {
	/**
	 * Static list to reduce equal queries
	 *
	 * @var null
	 */
	private static $all_post_ids = null;

	/**
	 * Returns an ImageObject instance with data from the post thumbnail
	 *
	 * @param int $post_id ID of the post to get thumbnail from.
	 *
	 * @return ImageObject Image data object
     */
	public static function get_post_thumbnail_data( $post_id ) {
		$feat_image = null;
		if ( has_post_thumbnail( $post_id ) ) {
			$feat_image = new ImageObject( get_post_thumbnail_id( $post_id ) );
		}
		return $feat_image;
	}


	/**
	 * Returns author data for the post
	 *
	 * @param int $post_id ID of the post to get author data.
	 *
	 * @return array
	 */
	public static function get_post_author_data( $post_id ) {
		$author_id = get_post_field( 'post_author', $post_id );

		return array(
			'id'    => get_the_author_meta( 'ID', $author_id ),
			'name'  => get_the_author_meta( 'display_name', $author_id ),
			'image' => get_avatar( get_the_author_meta( 'ID', $author_id ) ),
		);
	}

	/**
	 * Static query for all posts.
	 *
	 * @param array $post_status Will include posts from the provided status, i.e. array( 'publish', 'draft', 'future' ).
	 *
	 * @return int[]|\WP_Post[]|null
	 */
    public static function get_all_post_ids( array $post_status = array( 'publish' ) ) {
		if ( ! self::$all_post_ids ) {
			self::$all_post_ids = get_posts(
				array(
					'numberposts' => -1,
					'type'        => 'post',
					'post_status' => $post_status,
					'fields'      => 'ids',
				)
			);
		}
		return self::$all_post_ids;
	}

	/**
     * Calc the estimated reading time for an article, based on the amount of words in the content.
     *
     * @param string $content Content of the article.
     * @return int Time in minutes
     */
    public static function get_post_reading_time( $content ): int {
		$words_count      = str_word_count( wp_strip_all_tags( $content ) );
		$words_per_minute = 220;
		$total_read_time  = $words_count / $words_per_minute;
		return ceil( $total_read_time );
	}

	/**
	 * Returns the WP_Term object from the post primary category
	 *
	 * @param int $post_id ID of the post.
	 * @return WP_Term
	 */
    public static function get_post_primary_category( $post_id ) {
		if ( ! $post_id ) {
			return null;
		}

		$primary_id = intval( get_post_meta( $post_id, '_yoast_wpseo_primary_category', true ) );
		return get_category( $primary_id );
	}

	/**
     * Returns array of breadcrumbs of an article.
     *
     * @param int $post_id ID of the post.
     * @return object Object with 'name' and 'path' properties
     */
    public static function get_post_breadcrumbs( $post_id ) {
		// Get parent categories
		$primary_category = self::get_post_primary_category( $post_id );
		$categories       = array();
		if ( $primary_category ) {
			$categories[] = $primary_category;
			if ( $primary_category->parent > 0 ) {
				$categories = array_merge( $categories, TaxonomyUtils::get_term_ascendancy( $primary_category->term_id, 'category' ) );
			}
        }

		// Build the breadcrumb objects
		$breadcrumbs = array();
		foreach ( array_reverse( $categories ) as $category ) {
			$breadcrumbs[] = (object) array(
				'title' => $category->name,
				'path'  => ThemeUtils::get_relative_path( get_category_link( $category ) ),
			);
		}
		return $breadcrumbs;
    }
}