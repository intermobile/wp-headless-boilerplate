<?php

namespace Theme\Tests;

use Theme\Helpers\TestObject;
use Theme\Helpers\ArticleUtils;
use Theme\Helpers\ThemeUtils;

/**
 * Class Post for Tests
 *
 * @package Theme\Tests
 */
class PostTests {

	/**
	 * Type of object for TestObject context
	 *
	 * @var string
	 */
	private $context = 'post';

	/**
	 * Check if there is any article marked as Uncategorized
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_uncategorized_articles( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = true;
		$response->description  = 'Posts shouldn\'t use the “Uncategorized” category';
		$response->message      = 'Make sure all posts are associated with at least 1 category and 1 subcategory, and no one has the <code>Uncategorized</code> default category assigned.';
		// all articles marked as Uncategorized (ID: 1)
		$posts = get_posts(
			array(
				'numberposts' => -1,
				'type'        => 'post',
				'post_status' => 'publish',
				'cat'         => 1,
			)
		);
		if ( count( $posts ) ) {
			foreach ( $posts as $post ) {
				$url = sprintf( '%s?action=edit&post=%s', admin_url( 'post.php' ), $post->ID );
				$response->add_result( $post->ID, $post->post_title, $url );
			}
		}
		return $response;
	}

	/**
	 * Check if all published articles have 1 category and 1 subcategory
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_correct_categorization( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = true;
		$response->description  = 'Posts should have a category and subcategory selected';
		$response->message      = 'Make sure all published posts are assigned to at least 1 parent category and 1 child category (from the selected parent).';
		// all articles
		$posts = ArticleUtils::get_all_post_ids( isset( $args['post_status'] ) ? $args['post_status'] : array( 'publish' ) );
		foreach ( $posts as $post ) {
			$categories = get_the_category( $post );
			$cat_ok     = false;
			$subcat_ok  = false;
			foreach ( $categories as $cat ) {
				if ( 0 === $cat->parent ) {
					$cat_ok = true;
				} elseif ( $cat->parent > 0 ) {
					$subcat_ok = true;
				}
			}
			if ( ! $cat_ok || ! $subcat_ok ) {
				$url   = sprintf( '%s?action=edit&post=%s', admin_url( 'post.php' ), $post );
				$title = get_the_title( $post );
				$response->add_result( $post, $title, $url );
			}
		}
		return $response;
	}

	/**
	 * Check if the primary category of all published articles is a subcategory
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_primary_category( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = true;
		$response->description  = 'Posts should have a subcategory defined as primary';
		$response->message      = 'Make sure all published posts have a child category set as the primary category.';

		// Get all articles
		$posts = ArticleUtils::get_all_post_ids( isset( $args['post_status'] ) ? $args['post_status'] : array( 'publish' ) );
		foreach ( $posts as $post ) {
			$primary_cat = (int) get_post_meta( $post, '_yoast_wpseo_primary_category', true );
			$categories  = get_the_category( $post );
			$primary_ok  = false;
			foreach ( $categories as $cat ) {
				if ( $cat->parent > 0 && $cat->term_id === $primary_cat ) {
					$primary_ok = true; // subcategory is a primary
				}
			}
			if ( ! $primary_ok ) {
				$url   = sprintf( '%s?action=edit&post=%s', admin_url( 'post.php' ), $post );
				$title = get_the_title( $post );
				$response->add_result( $post, $title, $url );
			}
		}
		return $response;
	}

	/**
	 * Check whether all posts have a JPG image as featured
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_featured_image( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = true;
		$response->description  = 'Posts should have a JPG image defined as featured media';
		$response->message      = 'Make sure all published posts have a featured image properly defined, and the image is <code>.jpg</code> or <code>.jpeg</code>, as other formats (like <code>.png</code>) are not allowed.';

		// Get all articles
		$posts = ArticleUtils::get_all_post_ids( isset( $args['post_status'] ) ? $args['post_status'] : array( 'publish' ) );

		foreach ( $posts as $post ) {
			$image_url = get_the_post_thumbnail_url( $post );
			$is_jpg    = preg_match( '/(\.jpg|\.jpeg)$/', $image_url );
			if ( ! $image_url || ! $is_jpg ) {
				$url   = sprintf( '%s?action=edit&post=%s', admin_url( 'post.php' ), $post );
				$title = get_the_title( $post );
				$response->add_result( $post, $title, $url );
			}
		}
		return $response;
	}

	/**
	 * Check whether are posts with scripts inside the content
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_content_scripts( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = false;
		$response->description  = 'Posts shouldn\'t contain script tags in the content';
		$response->message      = 'Make sure no posts contain script tags (i.e. <code>&lt;script src="…"&gt;</code>) inside the content. If an embed is adding scripts, please request the dev team to implement the script dynamically in the site.';

		// Get all articles
		$posts = ArticleUtils::get_all_post_ids( isset( $args['post_status'] ) ? $args['post_status'] : array( 'publish' ) );
		foreach ( $posts as $post ) {
			$content     = get_the_content( null, false, $post );
			$has_scripts = preg_match( '/<script\b/', $content );
			if ( $has_scripts ) {
				$url   = sprintf( '%s?action=edit&post=%s', admin_url( 'post.php' ), $post );
				$title = get_the_title( $post );
				$response->add_result( $post, $title, $url );
			}
		}
		return $response;
	}

	/**
	 * Check the meta description in all posts
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_seo_meta_description( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = false;
		$response->description  = 'Posts should have a meta description';
		$response->message      = 'Make sure all published posts have the meta description field properly filled in the <b>Yoast SEO</b> section.';
		// Get all articles
		$posts      = ArticleUtils::get_all_post_ids( isset( $args['post_status'] ) ? $args['post_status'] : array( 'publish' ) );
		$result_ids = array();
		if ( count( $posts ) ) {
			$result_ids = ThemeUtils::get_seo_meta_description( $posts, 'post', 'post' );
		}
		foreach ( $posts as $post ) {
			// if the yoast object_id of was not found for this post
			if ( ! in_array( $post, $result_ids ) ) {
				$url   = sprintf( '%s?action=edit&post=%s', admin_url( 'post.php' ), $post );
				$title = get_the_title( $post );
				$response->add_result( $post, $title, $url );
			}
		}
		return $response;
	}
}