<?php

namespace Theme\Tests;

use Theme\Helpers\TestObject;
use Theme\Helpers\ThemeUtils;

/**
 * Class Category for Tests
 *
 * @package Theme\Tests
 */
class PageTests {

	/**
	 * Type of object for TestObject context
	 *
	 * @var string
	 */
	private $context = 'page';

	/**
	 * Check if pages have SEO meta description
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_seo_meta_description( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = false;
		$response->description  = 'Pages should have a meta description';
		$response->message      = 'Make sure all pages have the meta description field properly filled in the <b>Yoast SEO</b> section.';
		// get all pages published
		$pages      = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => 'page',
				'post_status' => 'publish',
			)
		);
		$result_ids = array();
		if ( count( $pages ) ) {
			$array_pages = array();
			foreach ( $pages as $p ) {
				$array_pages[] = $p->ID;
			}
			$result_ids = ThemeUtils::get_seo_meta_description( $array_pages, 'post', 'page' );
		}
		foreach ( $pages as $pg ) {
			// if the yoast object_id of was not found for this post
			if ( ! in_array( $pg->ID, $result_ids ) ) {
				$url = sprintf( '%s?post=%s&action=edit', admin_url( 'post.php' ), $pg->ID );
				$response->add_result( $pg->ID, $pg->post_title, $url );
			}
		}
		return $response;
	}
}