<?php

namespace Theme\Tests;

use Theme\Helpers\TestObject;
use Theme\Helpers\ThemeUtils;

/**
 * Conformance tests for categories
 *
 * @package Theme\Tests
 */
class CategoryTests {

	/**
	 * Type of object for TestObject context
	 *
	 * @var string
	 */
	private $context = 'category';

	/**
	 * Check if all parent categories have mandatory fields filled in.
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_parent_category_settings( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = true;
		$response->description  = 'Parent categories should have a color and icon defined';
		$response->message      = 'Make sure all parent categories have the <code>Icon</code> and <code>Color</code> fields defined.';
		$terms                  = get_terms(
            array(
				'taxonomy'   => 'category',
				'parent'     => 0,
				'hide_empty' => false,
				'exclude'    => 1,
            )
        );
		if ( count( $terms ) ) {
			foreach ( $terms as $category ) {
				$color = get_field( 'style_color', "category_{$category->term_id}" );
				$icon  = get_field( 'style_icon', "category_{$category->term_id}" );
				if ( ! $color || ! $icon ) {
					$url = sprintf( '%s?taxonomy=category&post_type=post&tag_ID=%s', admin_url( 'term.php' ), $category->term_id );
					$response->add_result( $category->term_id, $category->name, $url );
				}
			}
		}
		return $response;
	}

	/**
	 * Check the meta description in all categories
	 *
	 * @param array $args Additional settings passed from the conformance panel.
	 *
	 * @return TestObject
	 */
	public function check_seo_meta_description( $args = array() ) {
		$response               = new TestObject();
		$response->context      = $this->context;
		$response->is_mandatory = false;
		$response->description  = 'Categories should have a meta description';
		$response->message      = 'Make sure all categories have the meta description field properly filled in the <b>Yoast SEO</b> section.';
		$result_ids             = array();
		$terms                  = get_terms(
			array(
				'taxonomy'   => 'category',
				'hide_empty' => false,
				'exclude'    => 1,
			)
		);
		if ( count( $terms ) ) {
			$array_terms = array();
			foreach ( $terms as $category ) {
				$array_terms[] = (int) $category->term_id;
			}
			$result_ids = ThemeUtils::get_seo_meta_description( $array_terms, 'term', 'category' );
		}
		foreach ( $terms as $category ) {
			// if the yoast object_id of was not found for this post
			if ( ! in_array( (int) $category->term_id, $result_ids ) ) {
				$url = sprintf( '%s?taxonomy=category&post_type=post&tag_ID=%s', admin_url( 'term.php' ), $category->term_id );
				$response->add_result( $category->term_id, $category->name, $url );
			}
		}
		return $response;
	}
}