<?php
namespace Theme\Pages;

use Theme\Helpers\PageUtils;

/**
 * Object model for the page_data field for pages using the default template
 */
class DefaultPageData {
    /**
     * Array of parent pages
     *
     * @var array
     */
    public $breadcrumbs;

	/**
     * ID of the current page
     *
     * @var int
     */
    protected $ID;

	/**
	 * Construct the data to return in the page_data field.
	 *
	 * @param int|WP_Post $page The page ID or object. If not defined, gets the page in context.
	 */
    public function __construct( $page = null ) {
		// Get page ID
        if ( is_numeric( $page ) ) {
            $this->ID = (int) $page;
        } elseif ( is_object( $page ) ) {
            $this->ID = $page->ID;
        } else {
            $this->ID = get_the_ID();
        }

		// Get content for page_data
        $this->breadcrumbs = PageUtils::get_page_breadcrumbs( $this->ID );
    }
}