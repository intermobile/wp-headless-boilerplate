<?php
namespace Theme\Pages;

use Theme\Helpers\ArticleQuery;

/**
 * Object model for the page_data field for the homepage
 */
class HomePageData extends DefaultPageData {
	/**
	 * List of articles data.
	 *
	 * @var ArticleObject[]
	 */
	public $latest_articles;

	/**
	 * Construct the data to return in the page_data field.
	 *
	 * @param int|WP_Post $page The page ID or object. If not defined, gets the page in context.
	 */
    public function __construct( $page = null ) {
		parent::__construct( $page );

		// Get content for page_data
        $this->latest_articles = ArticleQuery::get_articles( 6 );
    }
}