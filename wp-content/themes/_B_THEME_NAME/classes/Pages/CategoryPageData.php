<?php
namespace Theme\Pages;

use Theme\Helpers\CategoryUtils;
use Theme\Helpers\TaxonomyObject;

/**
 * Object model for the page_data field for categories
 */
class CategoryPageData {
	/**
     * Array of parent categories
     *
     * @var array
     */
    public $breadcrumbs;

	/**
     * Lists with each child category and some posts
     *
     * @var TaxonomyObject[]
     */
    public $subcategories;

	/**
     * ID of the current category
     *
     * @var int
     */
    protected $ID;

	/**
     * List of post IDs that were already listed on a section of the page.
     *
     * @var int[]
     */
    protected $excludes;

	/**
	 * Construct the data to return in the page_data field.
	 *
	 * @param int|WP_Term $category The category ID or object.
	 */
    public function __construct( $category ) {
		// Get category ID
		if ( is_numeric( $category ) ) {
			$this->ID = (int) $category;
		} elseif ( is_object( $category ) ) {
			$this->ID = $category->ID;
		} else {
			return null;
		}
		$this->excludes = array();

		// Get content for page_data
		$subcategories       = CategoryUtils::get_child_categories( $this->ID );
		$this->subcategories = array();
		foreach ( $subcategories as $category ) {
			$subcategory           = new TaxonomyObject( $category );
			$this->subcategories[] = $subcategory;
		}
		$this->breadcrumbs = CategoryUtils::get_category_breadcrumbs( $this->ID );
    }
}