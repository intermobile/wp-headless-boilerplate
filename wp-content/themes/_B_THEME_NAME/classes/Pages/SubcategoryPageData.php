<?php
namespace Theme\Pages;

use Theme\Helpers\TaxonomyUtils;

/**
 * Object model for the page_data field for child categories
 */
class SubcategoryPageData {
	/**
     * Array of parent categories
     *
     * @var array
     */
    public $breadcrumbs;

	/**
     * ID of the current category
     *
     * @var int
     */
    protected $ID;

	/**
	 * Construct the data to return in the page_data field.
	 *
	 * @param int|WP_Term $category The category ID or object.
	 */
    public function __construct( $category ) {
		// Get category ID
		if ( is_numeric( $category ) ) {
			$this->ID = (int) $category;
		} elseif ( is_object( $category ) ) {
			$this->ID = $category->ID;
		} else {
			return null;
		}

		// Get content for page_data
		$this->breadcrumbs = TaxonomyUtils::get_category_breadcrumbs( $this->ID );
    }
}
