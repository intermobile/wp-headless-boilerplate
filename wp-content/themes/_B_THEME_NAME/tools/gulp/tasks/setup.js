/**
 * A Gulp task for downloading the latest WordPress version and organizing its files
 * @param {*} gulp Instance of gulp
 * @param {object} config Task configurations
 * @param {object} project Project info
 * @param {object} hp Additional helper functions already imported on the gulpfile
 * @param {object} arg Object with arguments passed via command line
 */
module.exports = function (gulp, config, project, hp, arg) {
	return function () {
		// Import modules
		const prompt = require('inquirer');
		const zip = require('gulp-vinyl-zip');
		const del = require('del');
		const remoteSrc = require('gulp-remote-src');
		const mysql = require('mysql');
		const replace = require('gulp-replace');
		const rename = require('gulp-rename');
		const shell = require('shelljs');
		// const axios = require('axios');

		// Spinner used for loading messages
		let spinner = null;

		// Local paths and config
		let localConfig = {};

		// Progress holder
		let setupHistory = [];

		// Get the project name and directory
		let projectPathArray = project.path.split(/\/|\\/g);
		const projectFolderName = projectPathArray[projectPathArray.length - 4];
		projectPathArray = projectPathArray.splice(0, projectPathArray.length - 4);
		const projectDir = projectPathArray.join('/');

		return new Promise(async (done, reject) => {
			try {
				// Get local config
				await getLocalConfig();

				// Get progress of previous executions
				setupHistory = await getPreviousProgress();

				// Ask if running command as admin
				const { isRunningAsAdmin } = await askIfRunningAsAdmin();

				// Ask which tasks to run
				const { tasksToRun } = await askWhichTasksToRun(isRunningAsAdmin);

				// Download latest WordPress
				if (tasksToRun.includes('wordpress')) {
					await downloadWordPress();
					await unzipWordPress();
					await removeUnnecessaryFiles();
					await copyWordPress();
					await cleanWordPressFiles();
				}

				// Set up a wp-config.php file with default configuration and local db info
				if (tasksToRun.includes('wpconfig')) {
					await setUpWpConfig();
				}

				// Set up a .htaccess file with default configuration
				if (tasksToRun.includes('htaccess')) {
					await setUpHtaccess();
				}

				// Create a new database on the MySQL server
				if (tasksToRun.includes('database')) {
					await createDatabase();
				}

				// Add the local domain to the hosts file
				if (tasksToRun.includes('host')) {
					await addToHostFile();
				}

				// Add the local domain to the virtual hosts file
				if (tasksToRun.includes('vhost')) {
					await addVirtualHost();
				}

				// Run composer command to install all PHP depecencies
				if (tasksToRun.includes('composer')) {
					await installDependencies();
				}

				// All tasks were completed
				if (!tasksToRun.length) {
					console.log(hp.colors.green('✔ Done'), hp.colors.gray('- Nothing was changed'));
				} else {
					console.log(hp.colors.green('✔ Done'), hp.colors.gray('- All tasks completed successfully'));

					if (!isRunningAsAdmin && (!setupHistory.includes('host') || !setupHistory.includes('vhost'))) {
						console.log(
							hp.colors.yellow(
								'! Run the task again as admin/sudo to set up local host for this project.'
							)
						);
					}
				}
				done();
			} catch (err) {
				console.log(err);
				console.log(
					hp.colors.red('Unable to finish installation. Check the logs above to identify the problem.')
				);
				done();
			}
		});

		/**
		 * Create and starts a new spinner with the given text
		 * @param {string} message Text to be displayed in the log
		 */
		function startSpinner(message) {
			spinner = hp.ora({
				text: message,
				color: 'green',
			});
			spinner.start();
		}

		/**
		 * Finishes the current spinner with as succeeded
		 * @param {string} message Text to be displayed in the log
		 */
		function succeedSpinner(message) {
			spinner.succeed(hp.colors.green(message));
			spinner.clear();
		}

		/**
		 * Finishes the current spinner with as failed
		 * @param {string} message Text to be displayed in the log
		 */
		function failSpinner(message) {
			spinner.fail(hp.colors.red(message));
			spinner.clear();
		}

		/**
		 * Reset the spinner class
		 */
		function resetSpinner() {
			spinner = null;
		}

		/**
		 * Return sanitized string to be used on a RegEx pattern
		 * @param {string} string Text to sanitize
		 * @returns {string}
		 */
		function regexSanitize(string) {
			return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
		}

		function getPreviousProgress() {
			return new Promise((resolve, reject) => {
				const progressFile = hp.path.resolve(project.path, `${config.tmp}/setup-history`);

				// Check for existence of the config file
				if (!hp.fs.existsSync(progressFile)) {
					resolve([]);
					return;
				}

				// Return content of progress file
				const progress = JSON.parse(hp.fs.readFileSync(progressFile));
				resolve(progress);
			});
		}

		function saveCurrentProgress(completedTask) {
			return new Promise((resolve, reject) => {
				const tmpDir = hp.path.resolve(project.path, config.tmp);
				const progressFile = hp.path.resolve(tmpDir, 'setup-history');

				// Add new completed task to the progress list
				setupHistory.push(completedTask);

				// Create the tmp folder, if it doesn't exist
				if (!hp.fs.existsSync(tmpDir)) {
					hp.fs.mkdirSync(tmpDir);
				}

				// Writes the tmp file with current progress list
				try {
					hp.fs.writeFileSync(progressFile, JSON.stringify(setupHistory));
				} catch (err) {
					reject(err);
				}

				resolve();
			});
		}

		/**
		 * Get settings from local config file
		 * @returns {Promise}
		 */
		function getLocalConfig() {
			return new Promise(async (resolve, reject) => {
				const configFile = hp.path.resolve(require('os').homedir(), '.wpsetup.json');

				// Check for existence of the config file
				if (!hp.fs.existsSync(configFile)) {
					// Ask if should create config file
					const { createConfig } = await prompt.prompt({
						type: 'confirm',
						message: 'No config file was found in your home directory. Should we create one?',
						name: 'createConfig',
						default: true,
					});

					if (createConfig) {
						// Creates a default config file in your home directory
						try {
							hp.fs.writeFileSync(
								configFile,
								JSON.stringify(
									{
										mysqlHost: 'localhost',
										mysqlUser: '',
										mysqlPassword: '',
										hostsFile: '',
										vhostsFile: '',
										errorLogDir: '',
										accessLogDir: '',
									},
									null,
									'\t'
								)
							);

							console.log(hp.colors.green(`✔ The config file was successfully created at ${configFile}`));
							console.log(
								hp.colors.yellow(
									'! Please update the file with your MySQL credentials and hosts files path.'
								)
							);
							console.log(
								hp.colors.yellow(
									`! Leave the ${hp.colors.cyan('errorLogDir')} and ${hp.colors.cyan(
										'accessLogDir'
									)} empty to use the default settings, if you prefer.`
								)
							);

							// Wait for update confirmation
							const { continueSetup } = await prompt.prompt({
								type: 'confirm',
								message: `Press confirm when your config file is properly updated`,
								name: 'continueSetup',
								default: true,
							});
							resolve();
						} catch (err) {
							reject(err);
							return;
						}
					} else {
						console.log(
							hp.colors.yellow(
								`! Tasks that need configurations from the config file won't be shown in the options below.`
							)
						);
						resolve();
						return;
					}
				}

				// Get content of the config file
				try {
					localConfig = JSON.parse(hp.fs.readFileSync(configFile));
					console.log(
						hp.colors.gray(
							`! Config file found at ${configFile} - Tasks below will be using its configurations.`
						)
					);
					resolve();
				} catch (err) {
					failSpinner(spinnerText);
					reject("Your config file doesn't seem to be a valid JSON.");
				}
			});
		}

		/**
		 * Ask user if running the gulp setup task as admin (sudo)
		 * @returns {Promise}
		 */
		function askIfRunningAsAdmin() {
			return prompt.prompt({
				type: 'confirm',
				message: `Are you running this task as admin/sudo?`,
				name: 'isRunningAsAdmin',
				default: false,
			});
		}

		/**
		 * Ask user for which tasks should be executed, checking availability
		 * @param {boolean} isAdmin Whether
		 * @returns {Promise}
		 */
		async function askWhichTasksToRun(isAdmin = false) {
			return prompt.prompt({
				type: 'checkbox',
				message: `Which tasks do you want to run?`,
				choices: () => {
					const choices = [];

					if (isAdmin) {
						// Add Host Insertion option
						if (localConfig.hasOwnProperty('hostsFile') && localConfig.hostsFile != '') {
							choices.push({
								name: `Update Hosts File${hp.colors.gray('\tRequires hosts file path (hostsFile)')}`,
								short: 'Update Hosts File',
								value: 'host',
								checked: !setupHistory.includes('host'),
							});
						}

						// Add Virtual Host Insertion option
						if (localConfig.hasOwnProperty('vhostsFile') && localConfig.vhostsFile != '') {
							choices.push({
								name: `Add Virtual Host${hp.colors.gray(
									'\tRequires vhosts paths (vhostsFile, [errorLogDir, accessLogDir])'
								)}`,
								short: 'Add Virtual Host',
								value: 'vhost',
								checked: !setupHistory.includes('vhost'),
							});
						}
					} else {
						// Add WordPress Download option
						choices.push({
							name: `Download WordPress${hp.colors.gray('\tRequires network connection')}`,
							short: 'Download WordPress',
							value: 'wordpress',
							checked: !setupHistory.includes('wordpress'),
						});

						// Add wp-config setup option
						choices.push({
							name: `Set up wp-config${hp.colors.gray(
								`\tRequires default wp-config template (${config.wpconfig})`
							)}`,
							short: 'Set up wp-config',
							value: 'wpconfig',
							checked: !setupHistory.includes('wpconfig'),
						});

						// Add htaccess setup option
						choices.push({
							name: `Set up htaccess${hp.colors.gray(
								`\tRequires default htaccess template (${config.htaccess})`
							)}`,
							short: 'Set up htaccess',
							value: 'htaccess',
							checked: !setupHistory.includes('htaccess'),
						});

						// Add Database Creation option
						if (
							localConfig.hasOwnProperty('mysqlHost') &&
							localConfig.mysqlHost != '' &&
							localConfig.hasOwnProperty('mysqlUser') &&
							localConfig.mysqlUser != '' &&
							localConfig.hasOwnProperty('mysqlPassword')
						) {
							choices.push({
								name: `Create Database${hp.colors.gray(
									'\tRequires MySQL credentials (mysqlHost, mysqlUser, mysqlPassword)'
								)}`,
								short: 'Create Database',
								value: 'database',
								checked: !setupHistory.includes('database'),
							});
						}

						// Add PHP dependencies installation option
						if (
							hp.fs.existsSync(hp.path.resolve(project.path, './composer.json')) &&
							shell.which('composer')
						) {
							choices.push({
								name: `Install Dependencies${hp.colors.gray('\tRequires composer installed')}`,
								short: 'Install Dependencies',
								value: 'composer',
								checked: !setupHistory.includes('composer'),
							});
						}
					}

					return choices;
				},
				name: 'tasksToRun',
			});
		}

		/**
		 * Download latest WordPress
		 * @returns {Promise}
		 */
		function downloadWordPress(done) {
			// Try download
			return new Promise((resolve, reject) => {
				// Start spinner message
				startSpinner('Downloading WordPress');

				hp.pump(
					[
						remoteSrc(['latest.zip'], {
							base: 'https://wordpress.org/',
						}).on('error', (err) => {
							spinner.fail();
							resetSpinner();
							throw new Error(err);
						}),
						gulp.dest('./').on('end', () => {
							spinner.succeed(hp.colors.green(spinner.text));
							spinner.clear();
							resolve();
						}),
					],
					done
				);
			});
		}

		/**
		 * Unzip WordPress folder
		 * @returns {Promise}
		 */
		function unzipWordPress(done) {
			return new Promise((resolve, reject) => {
				// Add spinner message
				const spinner = hp.ora({
					text: 'Unzipping WordPress',
					color: 'green',
				});
				spinner.start();

				hp.pump(
					[
						zip.src('./latest.zip').on('error', (err) => {
							spinner.fail();
							throw new Error(err);
						}),
						gulp.dest('./').on('end', () => {
							spinner.succeed(hp.colors.green('Unzipping WordPress'));
							spinner.clear();
							resolve();
						}),
					],
					done
				);
			});
		}

		/**
		 * Remove unnecessary files from WordPress, since the repository already have them
		 * @returns {Promise}
		 */
		function removeUnnecessaryFiles() {
			return new Promise((resolve, reject) => {
				del(['./wordpress/wp-content', './wordpress/readme.html', './wordpress/license.txt'])
					.then(() => {
						resolve();
					})
					.catch(reject);
			});
		}

		/**
		 * Copy WordPress files to the repository root
		 * @returns {Promise}
		 */
		function copyWordPress(done) {
			return new Promise((resolve, reject) => {
				// Add spinner message
				const spinner = hp.ora({
					text: 'Moving files',
					color: 'green',
				});
				spinner.start();

				hp.pump(
					[
						gulp.src('./wordpress/**/*').on('error', (err) => {
							spinner.fail();
							spinner.clear();
							throw new Error(err);
						}),
						gulp.dest('../../../').on('end', () => {
							spinner.succeed(hp.colors.green('Moving files'));
							spinner.clear();
							resolve();
						}),
					],
					done
				);
			});
		}

		/**
		 * Remove unnecessary and temporary files
		 * @returns {Promise}
		 */
		function cleanWordPressFiles(done) {
			return new Promise((resolve, reject) => {
				// Add spinner message
				const spinner = hp.ora({
					text: 'Cleaning files',
					color: 'green',
				});
				spinner.start();

				del(['./wordpress', './latest.zip'])
					.then(async () => {
						spinner.succeed(hp.colors.green('Cleaning files'));
						spinner.clear();
						await saveCurrentProgress('wordpress');
						resolve();
					})
					.catch((err) => {
						spinner.fail();
						spinner.clear();
						throw new Error(err);
					});
			});
		}

		/**
		 * Create a new wp-config.php with the proper config
		 * @returns {Promise}
		 */
		function setUpWpConfig(done) {
			return new Promise((resolve, reject) => {
				// Start spinner message
				const spinnerText = 'Setting up wp-config';
				startSpinner(spinnerText);

				// Check for existence of entry file
				if (!hp.fs.existsSync(hp.path.resolve(project.path, config.wpconfig))) {
					failSpinner(spinnerText);
					reject(`No source file was found at ${config.wpconfig}`);
					return;
				}

				// Define text to replace
				const db = {
					name: `define( 'DB_NAME', '${projectFolderName}_local' );`,
					user: `define( 'DB_USER', '${localConfig.mysqlUser}' );`,
					pass: `define( 'DB_PASSWORD', '${localConfig.mysqlPassword}' );`,
					host: `define( 'DB_HOST', '${localConfig.mysqlHost}' );`,
				};

				hp.pump(
					[
						// Get source file
						gulp.src(config.wpconfig),

						// Insert DB settings
						replace(/define\(\s?'DB_NAME',\s?'.*?'\s?\);/, db.name),
						replace(/define\(\s?'DB_USER',\s?'.*?'\s?\);/, db.user),
						replace(/define\(\s?'DB_PASSWORD',\s?'.*?'\s?\);/, db.pass),
						replace(/define\(\s?'DB_HOST',\s?'.*?'\s?\);/, db.host),

						// Rename destination file
						rename({
							basename: 'wp-config',
							extname: '.php',
						}),

						// Save file
						gulp.dest('../../../').on('end', async () => {
							succeedSpinner(spinnerText);
							await saveCurrentProgress('wpconfig');
							resolve();
						}),
					],
					done
				);
			});
		}

		function setUpHtaccess(done) {
			return new Promise((resolve, reject) => {
				// Start spinner message
				const spinnerText = 'Setting up htaccess';
				startSpinner(spinnerText);

				// Check for existence of entry file
				if (!hp.fs.existsSync(hp.path.resolve(project.path, config.htaccess))) {
					failSpinner(spinnerText);
					reject(`Problem: No ${hp.colors.cyan('htaccess')} source file was found at ${config.htaccess}`);
					return;
				}

				hp.pump(
					[
						// Get source file
						gulp.src(config.htaccess),

						// Rename destination file
						rename({
							basename: '.htaccess',
							extname: '',
						}),

						// Save file
						gulp.dest('../../../').on('end', async () => {
							succeedSpinner(spinnerText);
							await saveCurrentProgress('htaccess');
							resolve();
						}),
					],
					done
				);
			});
		}

		/** Create a new database */
		function createDatabase() {
			return new Promise((resolve, reject) => {
				// Start spinner message
				const spinnerText = 'Creating Database';
				startSpinner(spinnerText);

				// Create a connection with MySQL server
				const connection = mysql.createConnection({
					host: localConfig.mysqlHost,
					user: localConfig.mysqlUser,
					password: localConfig.mysqlPassword,
				});

				// Connect to MySQL
				connection.connect(function (err) {
					if (err) {
						failSpinner(spinnerText);
						if (err.hasOwnProperty('sqlMessage')) {
							reject(err.sqlMessage);
						} else {
							reject(err);
						}
						return;
					}

					// Create a new database
					const dbName = projectFolderName + '_local';
					connection.query(`CREATE DATABASE \`${dbName}\`;`, async function (error, results, fields) {
						if (error) {
							failSpinner(spinnerText);
							if (error.hasOwnProperty('sqlMessage')) {
								reject(error.sqlMessage);
							} else {
								reject(error);
							}
							connection.end();
							return;
						}

						succeedSpinner(spinnerText + hp.colors.gray(` - ${dbName}`));
						connection.end();
						await saveCurrentProgress('database');
						resolve();
					});
				});
			});
		}

		/**
		 * Install PHP dependencies with Composer
		 * @returns {Promise}
		 */
		function installDependencies() {
			return new Promise(async (resolve, reject) => {
				// Start spinner message
				const spinnerText = 'Installing Dependencies';
				startSpinner(spinnerText);

				// Run composer install command to install all PHP dependencies
				let composerExec = shell.exec('composer install', { silent: true });

				// Check if command ran successfully
				if (composerExec.code === 0) {
					succeedSpinner(spinnerText);
					await saveCurrentProgress('composer');
					resolve();
				} else {
					failSpinner(spinnerText);
					reject(
						`Problem: Unexpected issue trying to install dependencies. Try to run the command ${hp.colors.cyan(
							'composer install'
						)} separately.`
					);
				}
			});
		}

		/**
		 * Add the site local domain to the hosts file
		 * @returns {Promise}
		 */
		function addToHostFile() {
			return new Promise(async (resolve, reject) => {
				// Start spinner message
				const spinnerText = 'Updating Host File';
				startSpinner(spinnerText);

				// Check for existence of the hosts file
				if (!hp.fs.existsSync(localConfig.hostsFile)) {
					failSpinner(spinnerText);
					reject(`Problem: No file was found at ${localConfig.hostsFile}`);
					return;
				}

				const domain = `${projectFolderName}.local`;
				const ipAddress = '127.0.0.1';
				const textToAppend = `\n${ipAddress}\t\t${domain}`;

				// Get hosts file content and check if rule already exists
				const fileContent = hp.fs.readFileSync(localConfig.hostsFile);
				const pattern = `${regexSanitize(ipAddress)}.*?${regexSanitize(domain)}`;
				if (new RegExp(pattern).test(fileContent)) {
					succeedSpinner(hp.colors.gray(`${spinnerText} - rule already exists`));
					await saveCurrentProgress('host');
					resolve();
					return;
				}

				// Append the rule for the local domain to the hosts file
				try {
					hp.fs.appendFileSync(localConfig.hostsFile, textToAppend);
					succeedSpinner(spinnerText + hp.colors.gray(` - ${ipAddress} ${domain}`));
					await saveCurrentProgress('host');
					resolve();
				} catch (err) {
					failSpinner(spinnerText);
					reject('Problem: Unable to write to the file. Try running the task as admin/sudo.');
				}
			});
		}

		/**
		 * Add rules for the virtual host in the httpd-vhosts.config file
		 * @returns {Promise}
		 */
		function addVirtualHost() {
			return new Promise(async (resolve, reject) => {
				// Start spinner message
				const spinnerText = 'Adding Virtual Host';
				startSpinner(spinnerText);

				// Check for existence of the vhosts file
				if (!hp.fs.existsSync(localConfig.vhostsFile)) {
					failSpinner(spinnerText);
					reject(`Problem: No file was found at ${localConfig.vhostsFile}`);
					return;
				}

				const domain = `${projectFolderName}.local`;
				const defaultLogDir = `${projectDir}/${projectFolderName}/logs`;

				// Define the path for the error log file
				let errorLog = `${defaultLogDir}/${projectFolderName}-error.log`;
				if (localConfig.hasOwnProperty('errorLogDir') && localConfig.errorLogDir != '') {
					// Add trailing slash if not present
					if (localConfig.errorLogDir[localConfig.errorLogDir.length - 1] != '/') {
						localConfig.errorLogDir += '/';
					}
					errorLog = `${localConfig.errorLogDir}${projectFolderName}-error.log`;
				} else {
					// Check for existence of the logs folder
					if (!hp.fs.existsSync(`${defaultLogDir}`)) {
						hp.fs.mkdirSync(`${defaultLogDir}`);
					}
				}

				// Define the path for the error log file
				let accessLog = `${defaultLogDir}/${projectFolderName}-access.log`;
				if (localConfig.hasOwnProperty('accessLogDir') && localConfig.accessLogDir != '') {
					// Add trailing slash if not present
					if (localConfig.accessLogDir[localConfig.accessLogDir.length - 1] != '/') {
						localConfig.accessLogDir += '/';
					}
					accessLog = `${localConfig.accessLogDir}${projectFolderName}-access.log`;
				} else {
					// Check for existence of the logs folder
					if (!hp.fs.existsSync(`${defaultLogDir}`)) {
						hp.fs.mkdirSync(`${defaultLogDir}`);
					}
				}

				// Get vhosts file content and check if host definition already exists
				const fileContent = hp.fs.readFileSync(localConfig.vhostsFile);
				const pattern = `${regexSanitize(domain)}`;
				if (new RegExp(pattern).test(fileContent)) {
					succeedSpinner(
						hp.colors.gray(`${spinnerText} - definition already exists - `) +
							hp.colors.yellow('You may need to restart apache.')
					);
					await saveCurrentProgress('vhost');
					resolve();
					return;
				}

				// Append rules for the virtual host into the vhosts file
				try {
					const textToAppend = `${new RegExp(/(\n|\r\n)$/).test(fileContent) ? '' : '\n'}
<VirtualHost *:80>
\tServerName ${domain}
\tServerAlias www.${domain}
\tDocumentRoot "${projectDir}/${projectFolderName}"
\tErrorLog "${errorLog}"
\tCustomLog "${accessLog}" combined
</VirtualHost>\n`;
					hp.fs.appendFileSync(localConfig.vhostsFile, textToAppend);
					succeedSpinner(
						spinnerText +
							hp.colors.gray(' - ') +
							hp.colors.cyan(`http://${domain}/`) +
							hp.colors.gray(' - ') +
							hp.colors.yellow('Restart your apache server to access the local URL.')
					);
					resolve();
				} catch (err) {
					failSpinner(spinnerText);
					reject('Problem: Unable to write to the file. Try running the task as admin/sudo.');
				}
			});
		}
	};
};
