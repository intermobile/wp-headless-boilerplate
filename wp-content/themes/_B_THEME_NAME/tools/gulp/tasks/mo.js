/**
 * A Gulp task for compiling binary .mo files from .po translations
 * @param {*} gulp Instance of gulp
 * @param {object} config Task configurations
 * @param {object} project Project info
 * @param {object} hp Additional helper functions already imported on the gulpfile
 * @param {object} arg Object with arguments passed via command line
 */
module.exports = function (gulp, config, project, hp, arg) {
	return function () {
		// Import modules
		const po2mo = require('gulp-po2mo');

		return new Promise((done, reject) => {
			return hp.pump(
				[
					// Get srouce files
					gulp.src(config.files),

					// Compiles .mo files
					po2mo().on('error', reject),

					// Write files to destination folder
					gulp.dest(config.dist).on('end', function () {
						console.log(
							hp.colors.green(`\n✔ MO files successfully generated at ${hp.colors.cyan(config.dist)}.\n`)
						);
					}),
				],
				done
			);
		});
	};
};
