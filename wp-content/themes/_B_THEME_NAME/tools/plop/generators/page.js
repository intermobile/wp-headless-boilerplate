const slugify = require('slugify');
const colors = require('colors');

const createPage = (plop) => {
	plop.setGenerator('page', {
		description: 'Generate a new template for pages',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: `What is the template name?`,
				suffix: ' (separate words by dash or space)'.gray,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
			},
			{
				type: 'confirm',
				name: 'hasCustomFields',
				message: `Will this page contain ACFs?`,
				suffix: ' (if yes, a FieldGroup class will be created)'.gray,
				default: true,
			},
		],
		actions: (data) => {
			const actions = [];

			// Page template
			actions.push({
				type: 'add',
				templateFile: '../templates/page/template.hbs',
				path: `./templates/{{ kebabCase name }}.php`,
			});

			// PageData class and import
			actions.push({
				type: 'add',
				templateFile: '../templates/page/page-data.hbs',
				path: './classes/Pages/{{ pascalCase name }}PageData.php',
			});
			actions.push({
				type: 'modify',
				pattern: /(\s+default:\s+return new Pages\\Default)/i,
				templateFile: '../templates/page/page-return.hbs',
				path: './classes/Api/WpPages.php',
			});

			// Field Group class
			if (data.hasCustomFields) {
				actions.push({
					type: 'add',
					templateFile: '../templates/page/field-group.hbs',
					path: './classes/FieldGroups/{{ pascalCase name }}ContentFieldGroup.php',
				});
			}

			return actions;
		},
	});
};

module.exports = createPage;
