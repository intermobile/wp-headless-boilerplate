const slugify = require('slugify');
const colors = require('colors');

const createCustomField = (plop) => {
	plop.setGenerator('custom-field', {
		description: 'Generate a new CustomField class for grouping ACFs',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: `What is the class name?`,
				suffix: colors.gray(
					` (usually the page and section, i.e. ${'about hero'.magenta}, ${'contact form'.magenta}, etc.)`
				),
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
			},
		],
		actions: (data) => {
			const actions = [];
			data.type = 'customField';

			//Field File Class
			actions.push({
				type: 'add',
				templateFile: '../templates/custom-field/class.hbs',
				path: './classes/CustomFields/{{ pascalCase name }}Fields.php',
			});

			return actions;
		},
	});
};

module.exports = createCustomField;
