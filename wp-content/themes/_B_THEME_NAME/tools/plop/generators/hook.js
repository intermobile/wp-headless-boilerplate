const slugify = require('slugify');
const colors = require('colors');

const createHook = (plop) => {
	plop.setGenerator('hook', {
		description: 'Generate a new Hook class for registering actions and filters',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: `What is the Hook name?`,
				suffix: ' (separate words by dash or space)'.gray,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
			},
			{
				type: 'input',
				name: 'description',
				message: `Description:`,
				suffix: ' (describe the class meaning, i.e. Hooks for registering menus)'.gray,
				validate: (input) => {
					return input.length ? true : `Please add a description text.`.red;
				},
			},
		],
		actions: (data) => {
			const actions = [];

			// Hook class
			actions.push({
				type: 'add',
				templateFile: '../templates/hook/class.hbs',
				path: './classes/Hooks/{{ pascalCase name }}.php',
			});

			return actions;
		},
	});
};

module.exports = createHook;
