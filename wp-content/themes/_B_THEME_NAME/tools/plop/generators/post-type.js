const slugify = require('slugify');
const colors = require('colors');

const createPostType = (plop) => {
	plop.setGenerator('post-type', {
		description: 'Generate a class for registering a new post type',
		prompts: [
			{
				type: 'input',
				name: 'singularName',
				message: `What is the Post Type name?`,
				suffix: colors.gray(
					` (in singular, i.e. ${'product'.magenta}, ${'story'.magenta}, ${'faq'.magenta}, etc.)`
				),
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					if (!pattern.test(input)) {
						return `Should contain only letters, digits and dashes, and must start with a letter`.red;
					}

					const reserved = [
						'post',
						'page',
						'attachment',
						'revision',
						'nav_menu_item',
						'custom_css',
						'customize_changeset',
						'oembed_cache',
						'user_request',
						'wp_block',
						'action',
						'author',
						'order',
						'theme',
					];
					const snakeCase = input.replace(/[\s-]/g, '_').toLowerCase();
					if (reserved.includes(snakeCase)) {
						return `${snakeCase.bold} is reserved by WordPress, please choose another name`.red;
					}

					return true;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
						replacement: ' ',
					});
				},
			},
			{
				type: 'input',
				name: 'pluralName',
				message: `What is the name in plural?`,
				suffix: colors.gray(` (i.e. ${'products'.magenta}, ${'stories'.magenta}, ${'faqs'.magenta}, etc.)`),
				default: (answers) => `${answers.singularName}s`,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
						replacement: ' ',
					});
				},
			},
			{
				type: 'confirm',
				name: 'isHierarchical',
				message: `Is it hierarchical?`,
				suffix: ` (Whether it can have a parent or children)`.gray,
				default: false,
			},
			{
				type: 'confirm',
				name: 'showInRest',
				message: `Enable endpoint in the REST API?`,
				default: false,
			},
			{
				type: 'checkbox',
				name: 'supports',
				message: `Which features should be enabled?`,
				choices: [
					{
						name: 'title',
						checked: true,
					},
					'editor',
					'comments',
					'revisions',
					'trackbacks',
					'author',
					'excerpt',
					'page-attributes',
					'thumbnail',
					{
						name: 'custom-fields' + ' (native, not ACF)'.gray,
						short: 'custom-fields',
						value: 'custom-fields',
					},
					'post-formats',
				],
				loop: false,
				filter: (selected) => {
					return selected.length ? ` \'${selected.join("', '")}\' ` : '';
				},
			},
		],
		actions: (data) => {
			const actions = [];

			// Post type class
			actions.push({
				type: 'add',
				templateFile: '../templates/post-type/class.hbs',
				path: './classes/PostTypes/{{ pascalCase singularName }}.php',
				data: {
					dashicon: getSuggestedIcon(data.singularName),
				},
			});

			// Special cases that are better with uppercase labels
			if (['cta', 'faq'].includes(data.singularName)) {
				actions.push({
					type: 'modify',
					path: './classes/PostTypes/{{ pascalCase singularName }}.php',
					pattern: /(?<=_x\(\s'[^']*)(cta|faq)/gi,
					template: data.singularName.toUpperCase(),
					data: {
						dashicon: getSuggestedIcon(data.singularName),
					},
				});
			}

			return actions;
		},
	});
};

/**
 * Return a suggested icon if matching one of the common names
 * @param postType Name of the post type
 */
function getSuggestedIcon(postType) {
	const icons = {
		product: 'products',
		faq: 'editor-help',
		cta: 'button',
		question: 'editor-help',
		recipe: 'food',
		resource: 'open-folder',
		book: 'open-folder',
		testimonial: 'format-quote',
		story: 'format-quote',
		quotes: 'format-quote',
		ticket: 'tickets-alt',
		event: 'calendar-alt',
		member: 'groups',
		team: 'groups',
		course: 'welcome-learn-more',
		class: 'welcome-learn-more',
		ingredient: 'carrot',
		calculator: 'calculator',
		calc: 'calculator',
		battle: 'awards',
		competition: 'awards',
		quiz: 'analytics',
	};
	return 'dashicons-' + (icons.hasOwnProperty(postType) ? icons[postType] : 'star');
}

module.exports = createPostType;
