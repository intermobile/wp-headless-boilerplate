const slugify = require('slugify');
const colors = require('colors');

const createEndpoint = (plop) => {
	plop.setGenerator('endpoint', {
		description: 'Generate a new Endpoint class for extending the WP REST API',
		prompts: [
			{
				type: 'list',
				name: 'type',
				message: `Choose an endpoint type:`,
				choices: [
					{
						name: `${'WP'.bold} - Extends a native WP endpoint ${'(i.e. /wp/v2/posts)'.gray.bold}`,
						short: 'WP',
						value: 'wp',
					},
					{
						name: `${'Custom'.bold} - Register a new custom endpoint ${'(i.e. /site/v1/menus)'.gray.bold}`,
						short: 'Custom',
						value: 'custom',
					},
				],
				filter: (input) => {
					return input;
				},
			},
			{
				type: 'input',
				name: 'vendor',
				default: 'site',
				message: `What is the namespace vendor?`,
				suffix: ' (i.e. /'.gray.bold + 'vendor'.magenta.bold + '/v1/endpoint)'.gray.bold,
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.bold
						.red;
					return pattern.test(input) ? true : message;
				},
				when: (answers) => answers.type === 'custom',
			},
			{
				type: 'input',
				name: 'endpoint',
				message: `What is the endpoint name?`,
				suffix: ' (i.e. /vendor/v1/'.gray.bold + 'endpoint'.magenta.bold + ')'.gray.bold,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
				when: (answers) => answers.type === 'custom',
			},
			{
				type: 'input',
				name: 'endpoint',
				message: `What is the endpoint name?`,
				suffix: ' (i.e. /wp/v2/'.gray.bold + 'endpoint'.magenta.bold + ')'.gray.bold,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
				when: (answers) => answers.type === 'wp',
			},
			{
				type: 'input',
				name: 'objectType',
				message: `What is the object type?`,
				suffix: colors.bold.gray(
					` (A core type, like ${'user'.magenta}, ${'meta'.magenta}, ${'tag'.magenta}, ${
						'comment'.magenta
					}, or a custom post type)`
				),
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return input.toLowerCase();
				},
				when: (answers) => answers.type === 'wp',
			},
			{
				type: 'input',
				name: 'description',
				message: `Description:`,
				default: (answers) => {
					if (answers.type === 'custom') {
						return `Register a custom endpoint for returning the ${answers.vendor} ${answers.endpoint} data`;
					}
					return '';
				},
				validate: (input) => {
					return input.length ? true : `Please add a description text.`.red;
				},
				when: (answers) => answers.type === 'custom',
			},
		],
		actions: (data) => {
			const actions = [];

			// Endpoint class
			actions.push({
				type: 'add',
				templateFile: `../templates/endpoint/class-${data.type}.hbs`,
				path: `./classes/Api/{{ pascalCase className }}.php`,
				force: false,
				data: {
					className: `${data.type === 'custom' ? data.vendor : data.type}-${data.endpoint}`,
				},
			});

			return actions;
		},
	});
};

module.exports = createEndpoint;
