<?php
/**
 * Functions
 *
 * Setup functions and theme definitions
 *
 * @see https://developer.wordpress.org/themes/basics/theme-functions/
 * @see https://gitlab.com/intermobile/wp-solidify
 *
 * @package Theme
 */

use Solidify\Core\Theme;
use Solidify\Core\WPTemplate;

// Composer autoload
require get_template_directory() . '/vendor/autoload.php';

$registrable_namespaces = array();

// Check if ACF plugin is active to register fields
if ( function_exists( 'acf_add_local_field_group' ) ) {
	$registrable_namespaces[] = 'FieldGroups';
	$registrable_namespaces[] = 'Options';
}

// Set core registrables
$registrable_namespaces = array_merge(
    $registrable_namespaces,
    array(
		'Taxonomies',
		'PostTypes',
		'Hooks',
		'Api',
	)
);

// Setup a theme instance for WP Solidify
global $theme_class;
$theme_class = new Theme(
    array(
		'template_engine'        => new WPTemplate(),
		'namespace'              => 'Theme',
		'base_folder'            => 'classes',
		'registrable_namespaces' => $registrable_namespaces,
    )
);