var buildConformance = new Vue({
	el: '#build-conformance-page',

	data: {
		// Updated status
		status: null,
		infoMessage: '',
		errorMessage: '',
		timeOfLastDeploy: null,
		deployTimerInterval: null,
		timeOffset: 0,

		// Test results lists
		reprovedTests: [],
		warningTests: [],
		approvedTests: [],

		// Buttons
		isCheckEnabled: false,
		isDeployEnabled: false,
		hasCheckedOnce: false,
		includeScheduledPosts: false,
		includePendingPosts: false,

		// Accordion
		expandedAccordionId: null,

		// Timer
		timeDeploy: 3,
	},

	watch: {
		status: function (status) {
			switch (status) {
				case 'loaded': // Vue loaded, home screen
					this.infoMessage = 'Run a content check before building and deploying the site.';
					this.isCheckEnabled = true;
					this.isDeployEnabled = false;
					this.hasCheckedOnce = false;
					clearInterval(this.deployTimerInterval);
					break;

				case 'checking': // Running tests, waiting for API response
					this.infoMessage = 'Running tests...';
					this.isCheckEnabled = false;
					this.isDeployEnabled = false;
					break;

				case 'check_failed': // Received error from API
					this.infoMessage = 'Failed to run the checking.';
					this.isCheckEnabled = true;
					this.isDeployEnabled = false;
					break;

				case 'check_completed': // Received tests results from API
					if (this.reprovedTests.length) {
						this.infoMessage = 'Fix the reproved items below to proceed.';
					} else if (this.warningTests.length) {
						this.infoMessage = 'Some things can be improved, but you can still deploy the site.';
					} else {
						this.infoMessage = 'All right! Everything complies with the requirements.&nbsp;👍';
					}
					this.isCheckEnabled = true;
					this.isDeployEnabled = !this.reprovedTests.length;
					break;

				case 'deploying': // Requesting a site build/deploy
					this.infoMessage = 'Requesting website build...';
					this.isCheckEnabled = false;
					this.isDeployEnabled = false;
					break;

				case 'deploy_failed': // Build/deploy request failed
					this.infoMessage = 'Build request failed.';
					this.isCheckEnabled = true;
					this.isDeployEnabled = !this.reprovedTests.length;
					break;

				case 'deploy_completed': // Build/deploy request successful
					// Get exact time of latest deploy (now)
					this.timeOfLastDeploy = new Date().getTime();

					// Adds an offset to the time to compensate the oscillation
					this.timeOffset = 500 - (this.timeOfLastDeploy % 1000);
					this.timeOfLastDeploy += this.timeOffset;

					// Updates the message
					this.updateSuccessMessage();
					this.deployTimerInterval = setInterval(() => {
						this.updateSuccessMessage();
					}, 1000);

					this.isCheckEnabled = false;
					this.isDeployEnabled = false;
					break;

				default:
					break;
			}
		},
	},

	methods: {
		isStatus(status) {
			if (Array.isArray(status)) {
				return status.indexOf(this.status) > -1;
			} else {
				return status === this.status;
			}
		},

		runTests() {
			this.status = 'checking';
			this.expandedAccordionId = null;

			// Set the endpoint with checked post status
			let apiUrl = document.getElementById('build-conformance-page').dataset.endpoint;
			let status = ['publish'];
			if (this.includeScheduledPosts) status.push('future');
			if (this.includePendingPosts) status.push('pending');
			apiUrl += `&post_status=${status.join(',')}`;

			axios
				.get(apiUrl)
				.then((response) => {
					this.updateTestsLists(response.data);
					this.status = 'check_completed';
					this.hasCheckedOnce = true;
				})
				.catch((err) => {
					// TODO: Handle error on template
					this.status = 'check_failed';
					this.hasCheckedOnce = true;
					this.errorMessage = err.response.data.message;
					console.log(err);
				});
		},

		updateTestsLists(data) {
			this.reprovedTests = data
				.filter((test) => test.is_mandatory && test.results.length !== 0)
				.map((test, index) => {
					test.id = `reproved-${index}`;
					return test;
				});

			this.warningTests = data
				.filter((test) => !test.is_mandatory && test.results.length !== 0)
				.map((test, index) => {
					test.id = `warning-${index}`;
					return test;
				});

			this.approvedTests = data
				.filter((test) => test.results.length === 0)
				.map((test, index) => {
					test.id = `approved-${index}`;
					return test;
				});
		},

		resetTestsLists() {
			this.reprovedTests = [];
			this.warningTests = [];
			this.approvedTests = [];
		},

		triggerDeploy() {
			this.status = 'deploying';

			const webhookUrl = document.getElementById('deploy-webhook-url').value;
			axios
				.post(webhookUrl)
				.then((response) => {
					this.resetTestsLists();

					if ('job' in response.data && response.data.job.state === 'PENDING') {
						this.status = 'deploy_completed';
					} else {
						this.status = 'deploy_failed';
						this.errorMessage = `Didn't received job status.`;
					}
				})
				.catch((err) => {
					this.resetTestsLists();

					this.status = 'deploy_failed';
					this.errorMessage =
						(err.response.data && err.response.data.error && err.response.data.error.message) || err;
				});
		},

		toggleAccordion(accordionId) {
			this.expandedAccordionId = this.expandedAccordionId === accordionId ? null : accordionId;
		},

		returnToBeginning() {
			this.status = 'loaded';
		},

		updateSuccessMessage() {
			const now = new Date().getTime() + this.timeOffset;
			const secondsSinceLastDeploy = Math.floor((now - this.timeOfLastDeploy) / 1000);
			this.infoMessage = `Build requested successfully ${this.getFormattedTime(secondsSinceLastDeploy)} ago.`;
		},

		getFormattedTime(timeInSeconds = 0) {
			if (timeInSeconds < 60) {
				return `${timeInSeconds}s`;
			}
			if (timeInSeconds >= 60 * 60) {
				const hours = Math.floor(timeInSeconds / (60 * 60));
				const days = Math.floor(timeInSeconds / (60 * 60 * 24));
				return hours < 24 ? `${hours}h` : `${days}d`;
			}
			const minutes = Math.floor(timeInSeconds / 60);
			const seconds = timeInSeconds % 60;
			return seconds ? `${minutes}m ${seconds}s` : `${minutes}m`;
		},
	},

	created: function () {
		this.status = 'loaded';
	},
});
