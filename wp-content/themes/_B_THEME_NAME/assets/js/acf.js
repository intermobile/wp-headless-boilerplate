(function ($) {
	$(function () {
		// Print the ACF name when clicking its label
		$(document).on('click', '.acf-field label', function printAcfName(event) {
			const clicked = $(event.currentTarget);

			// Get the field and all its parents
			const names = [];
			clicked.parents('.acf-field, .acf-th').each((i, el) => {
				names.push($(el).attr('data-name'));
			});
			names.reverse();

			console.log(`%cACF:`, 'color:gray', `${names.join(' › ')}`);
		});
	});
})(jQuery);
