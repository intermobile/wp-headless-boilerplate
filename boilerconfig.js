module.exports = function (data) {
	return {
		replace: [
			{
				files: ["README.md", "wp-content/themes/_B_THEME_NAME/**/*"],
				ignore: [
					"wp-content/themes/_B_THEME_NAME/node_modules/**/*",
					"wp-content/themes/_B_THEME_NAME/vendor/**/*",
				],
				from: [
					/_B_PROJECT_NAME/g,
					/_B_FOLDER_NAME/g,
					/_B_THEME_NAME/g,
					/_B_CLIENT_NAME/g,
					/_B_SITE_URL/g,
					/_B_SITE_URI/g,
					/_B_CMS_URL/g,
					/_B_CMS_URI/g,
					/_B_AUTHOR_NAME/g,
					/_B_AUTHOR_URL/g,
					/_B_PACKAGE_NAME/g,
				],
				to: [
					data.title,
					data.folder,
					data.themeName,
					data.clientName,
					data.siteUrl,
					data.siteUri,
					data.cmsUrl,
					data.cmsUri,
					data.authorName,
					data.authorUrl,
					data.packageName,
				],
			},
			{
				files: ["README.md"],
				from: [
					/WP\sHeadless\sBoilerplate/g,
					/\\_B_PROJECT_NAME/g,
					/\\_B_CLIENT_NAME/g,
					/\\_B_SITE_URL/g,
					/\\_B_CMS_URL/g,
				],
				to: [
					data.title,
					data.title,
					data.clientName,
					data.siteUrl,
					data.cmsUrl,
				],
			},
		],
		rename: [
			{
				paths: [
					"wp-content/themes/_B_THEME_NAME/wp-headless-boilerplate.code-workspace",
				],
				from: "wp-headless-boilerplate",
				to: data.folder,
			},
			{
				paths: ["wp-content/themes/_B_THEME_NAME"],
				from: "_B_THEME_NAME",
				to: data.themeName,
			},
		],
		add: [],
		move: [],
		delete: [],
		run: [
			`cd wp-content/themes/${data.themeName}`,
			"npm install",
			"gulp setup",
		],
	};
};
