<!-- ![_B_PROJECT_NAME](wp-content/themes/_B_THEME_NAME/assets/img/_B_LOGO_NAME.png) -->

# WP Headless Boilerplate

This is a decoupled **WordPress** project developed for **\_B_PROJECT_NAME**, to **\_B_CLIENT_NAME**.

-   Website: [\_B_SITE_URL](_B_SITE_URI)
-   CMS: [\_B_CMS_URL](_B_CMS_URI)

## Summary

-   [Repository](#user-content-repository)
-   [Technologies](#user-content-technologies)
-   [Requirements](#user-content-requirements)
-   [Instalation](#user-content-instalation)
-   [Git Branches](#user-content-branches)
-   [Code Formatting](#user-content-formatting)
-   [Gulp Tasks](#user-content-gulp-tasks)
-   [Theme structure](#user-content-theme-structure)
-   [CSS](#user-content-css)
-   [Localization](#user-content-localization-translations)
-   [More info](#user-content-more-info)

## Repository

This git repository holds the WordPress root folder without the WordPress core files, including only the `themes`, `plugins` and `languages` folders. That means plugins are updated locally from the git repository.

> **Important**: The major part of this documentation is about the `_B_THEME_NAME` theme, that'ss where the website's code really lives. For further mentions, please consider the theme folder (`wp-content/themes/_B_THEME_NAME/`) as our root path.

#### .gitignore

We have two `.gitignore` files, one for the WordPress root, and other to our `_B_THEME_NAME` theme.

## Technologies

The project is mainly based on the following technologies:

-   [WordPress](https://developer.wordpress.org/) - CMS and API
-   [Composer](https://getcomposer.org/) - PHP dependency manager
-   [Advanced Custom Fields](https://www.advancedcustomfields.com/) - WP plugin used for registering custom fields in the CMS
-   [NPM](https://www.npmjs.com/) - For development CLIs, like **Gulp** and **Plop**
-   [jQuery](https://jquery.com/) - JS framework for the CMS
-   [Gulp](https://gulpjs.com/) - Task runner
-   [Plop](https://plopjs.com/) - Code files generator

## Requirements

To run this project on your local machine (or server), you will need:

-   [Apache Server](https://httpd.apache.org/download.cgi)
-   [PHP](https://www.php.net/downloads) (5+)
-   [MySQL Server](https://dev.mysql.com/downloads/mysql/) (5+)
-   [Composer](https://getcomposer.org/)
-   [Node.js](https://nodejs.org/en/) (including [npm](https://www.npmjs.com/))
-   [Gulp](https://gulpjs.com/) (installed globally via npm)

## Installation

There are 2 ways to set up the project in you local environment:

### Using the `gulp setup` task

1. Clone the repository on your machine;
2. In a terminal, navigate to the main theme folder (`_B_THEME_NAME`) and run `npm i` to install all JS dependencies;
3. Once finished, still in the terminal, run `gulp setup` (You need to have `gulp-cli` globally installed), to download WordPress, create local database, and install dependencies;
4. After this, run the task again as an admin user (sudo in Unix) to set up a virtual host for the local domain;
5. If you were able to run all steps above successfuly, you can now import the database (that should be provided to you) into the `_B_FOLDER_NAME_local` database, restart apache, and go to the `http://_B_FOLDER_NAME.local/` URL.

### Or manually

1. Clone the repository on your machine;
2. In a terminal, navigate to the main theme folder (`_B_THEME_NAME`) and run `npm i` to install all JS dependencies;
3. Download the [latest version](https://wordpress.org/latest.zip) of WordPress;
4. Add the following rule to your hosts file `127.0.0.1 _B_FOLDER_NAME.local`;
5. Setup a virtual host in Apache to the URL `http://_B_FOLDER_NAME.local/`;
6. Copy the content of the `.gulp/templates/wpconfig` file and create a `wp-config.php` file at the root of the project, inserting your MySQL credentials;
7. Create a new MySQL database named `_B_FOLDER_NAME_local` and import the database (that should be provided to you).
8. Restart apache and open the new local URL in your browser;

## Branches

At the time of the creation of this documentation we have 2 main branches, each one to a web environment:

1. `homolog` - Development environment
2. `master` - Production environment

Other branches are used to specific features and modifications.

## Formatting

For code formatting, this project uses:

-   [Prettier](https://prettier.io/) - That defines the formatting rules for `js`, `scss`, `css`, `json` and `html` files for the CMS front-end.
-   [PHP Code Beautifier and Fixer](https://phpqa.io/projects/phpcbf.html) - Also known as **phpcbf**, this controls how `php` files are formatted, by also fixing some possible issues accordingly to WordPress coding standard, using **PHP Code Sniffer**.

### VS Code

> For Front-End development, we highly recommend using the [Visual Studio Code](https://code.visualstudio.com/) IDE as this project already comes with some predefined rules and extension recommendations for this editor.

Specifically for VS Code, we have:

-   [Format HTML in PHP](https://marketplace.visualstudio.com/items?itemName=rifi2k.format-html-in-php) - A better code formatter for `html` inside `php` files.

> We high recommend opening the project through the **Workspace** file (`_B_FOLDER_NAME.code-workspace`) file, that comes with some formatting settings and recommended extensions.

## Gulp Tasks

The Gulp tasks are one of the main tools that the front-end developer can use.

To run a task just open a terminal, go to the `_B_THEME_NAME` theme directory and type the desired task command.

Some basic configurations for each tasks are defined in the `config.json` file (mainly file location).

See all the available tasks below:

> Make sure you have all needed dependencies installed to run the gulp tasks, using `npm install` or `npm i`.

### `gulp setup`

Does a series of tasks for setting up the project in your local machine.

> Some of its tasks may require specific definitions from you local environment, like MySQL credentials, hosts file location and more.

#### **As non-admin**

-   Downloads WordPress core files from latest version.
-   Setup `wp-config.php` and `.htaccess`
-   Create a MySQL database
-   Install PHP dependencies with Composer

#### **As admin/sudo**

-   Add rule for the local host domain
-   Add rule for a virtual host for Apache server

### `gulp sass`

Compiles all **SCSS** files under the `assets/scss` path to the `assets/css` folder.

#### Available Flags

-   `--watch`, `-w` - Watch for changes to the files and rerun the task when a file is modified (saved).
-   `--dev`, `-d` - Compiles with as unminified and with sourcemaps.

### `gulp pot`

Get all _gettext_ strings in all PHP files and create a `_B_THEME_NAME.pot` file to be used for translations.

### `gulp mo`

Compile a binary version of each translation file (`.po`).

### config.json

Every gulp task uses information from the `config.json`, like paths relative to the theme root directory. If you want to modify or consult the default directory for any gulp task you may want to check this file.

## Theme structure

Our `_B_THEME_NAME` theme, depends natively on 2 packages (_Composer_):

-   [Autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading) - That is used to automatically import any PHP class that is used in the theme's code.
-   [WP Solidify](https://gitlab.com/intermobile/wp-solidify) - Resposible for the core functionalities behind the back-end code, and the base PHP classes that are used along the whole theme.

In a nutshell, at the root of the theme, we have:

-   `funcions.php` - Where some definitions are made to the project, creating an instance of WP Solidify.
-   `templates` - Available page templates that allow different the pages to to have a specific set of custom fields.
-   `classes` - Most logic of the theme lives in this folder. Every file in this folder is a PHP class that controls an aspect of the site. Most of these classes somehow inherit from the `Registrable` class from **WP Solidify**. These files are organized by the following subfolders:
    -   `Api` - Customizations for the **WordPress REST API**, [extending](https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/) existing endpoints (prefixed with `Wp`), or [adding](https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/) new custom endpoints (usually prefixed with `Site`);
    -   `CustomFields` - Classes that are used to group multiple ACF fields registrations. These are used in the `FieldGroup` classes.
    -   `FieldGroups` - Registration of ACF fields groups.
    -   `Helpers` - Classes of reusable functions for the project's logic.
    -   `Hooks` - Classes where **action** and **filters** are registered for WordPress [hooks](https://developer.wordpress.org/plugins/hooks/).
    -   `Options` - Registration of new ACF option pages for the CMS.
    -   `PostTypes` - Registration of new [Custom Post Type](https://wordpress.org/support/article/post-types/#custom-post-types).
    -   `Taxonomies` - Registration of new [Taxonomies](https://wordpress.org/support/article/taxonomies/).
-   `assets` - Contains our theme **images**, **JS** and **CSS**.
-   `languages` - Where we have the `.po` and `.mo` files with translations for different languages.

#### Pages

We use the [WordPress template hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/) to define which route uses which file. All pages are read from the root directory but as it is a **decoupled** implementation, there is no content for the pages, they are just for template management.

## CSS (SCSS)

You can customize styles for the CMS, login and editor with the `.scss` files at `./assets/scss/`.

To compile the SCSS you need the [Sass CLI](https://sass-lang.com/documentation/cli/dart-sass) installed. Then run the command below at the **theme root folder**.

```bash
sass --watch --style=compressed --no-source-map assets/scss:assets/css
```

The command above will watch for changes in the SCSS files and update the files in the dist folder (`./assets/css/`).

## Localization / Translations

If the CMS needs to be available in more than one language, you can use `.po` files to localize text from the CMS.

All translations are placed in the `./languages` folder at the `_B_THEME_NAME` theme's root.

WordPress reads the binary `.mo` files, that needs to be generated using their respective `.po` files. Each .po file represents a language, and **needs to match** the language set in the CMS.

### Add new language

1. Run the `gulp pot` task to regenerate the main `_B_THEME_NAME.pot` file.
2. Create a copy of this file and rename it to the `[lang].po` (i.e. `pt_BR.po`, `es_AR.po`, `th.po`, etc)
3. Open the new `.po` file on the **_PoEdit_** app, fill the needed translations, and save the file.
4. Run the `gulp mo` task to regenerate the binary files.

> **Note:** _PoEdit_ already generates the binary `.mo` file when you save the `.po` file. However, it's recommended to use the `gulp mo` task anyway.

### Add new terms/translations

1. Run the `gulp pot` task to regenerate the main `_B_THEME_NAME.pot` file.
2. Update the `.po` lang file using the `_B_THEME_NAME.pot`, by opening the `.po` file on _PoEdit_, and go to `Catalog` > `Update from POT file...`, then select the `_B_THEME_NAME.pot` file.
3. Update the needed translations on _PoEdit_, and save the file.
4. Run the `gulp mo` task to regenerate the binary files.

### Update existing translations

1. Find the term you are looking for in the `.po` file, and change the translation text (`msgstr ""`).
2. Run the `gulp mo` task to regenerate the binary files.

## More information

For more information about this project get in touch with the development team or see the documentation related to the technology your are dealing with :)
